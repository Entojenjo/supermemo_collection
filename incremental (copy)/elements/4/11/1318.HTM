<strong><font color="blue"> : </font></strong><H4 id=_overview_9><A class=anchor href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#_overview_9"></A>12.97.1. Overview</H4>
<DIV class=paragraph>
<P>Varjo headsets provide extremely high pixel density displays in the center area of the display, blended with a high density display covering the rest of the field of view. If the application has to provide a single image per eye, that would cover the entire field of view, at the highest density it would be extremely resource intensive, and in fact impossible for the most powerful desktop GPUs to render in real time. So instead Varjo introduced the <CODE><A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XR_VARJO_quad_views">XR_VARJO_quad_views</A></CODE> extension enabling the application to provide two separate images for the two screen areas, resulting in a significant reduction in processing, for pixels that could not even been seen.</P></DIV>
<DIV class=paragraph>
<P>This extension goes a step further by enabling the application to only generate the density that can be seen by the user, which is another big reduction compared to the density that can be displayed, using dedicated eye tracking.</P></DIV>
<DIV class=paragraph>
<P>This extension requires <CODE><A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XR_VARJO_quad_views">XR_VARJO_quad_views</A></CODE> extension to be enabled.</P></DIV>
<DIV class=paragraph>
<P>An application using this extension to enable foveated rendering will take the following steps to prepare:</P></DIV>
<DIV class="olist arabic">
<OL class=arabic>
<LI>
<P>Enable <CODE><A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XR_VARJO_quad_views">XR_VARJO_quad_views</A></CODE> and <CODE><A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XR_VARJO_foveated_rendering">XR_VARJO_foveated_rendering</A></CODE> extensions.</P></LI>
<LI>
<P>Query system properties in order to determine if system supports foveated rendering.</P></LI>
<LI>
<P>Query texture sizes for foveated rendering.</P></LI></OL></DIV>
<DIV class=paragraph>
<P>In the render loop, for each frame, an application using this extension <STRONG class=purple>should</STRONG></P></DIV>
<DIV class="olist arabic">
<OL class=arabic>
<LI>
<P>Check if rendering gaze is available using <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#xrLocateSpace">xrLocateSpace</A>.</P></LI>
<LI>
<P>Enable foveated rendering when <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#xrLocateViews">xrLocateViews</A> is called.</P></LI></OL></DIV></DIV>
<DIV class=sect3>
<H4 id=_inspect_system_capability_2><A class=anchor href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#_inspect_system_capability_2"></A>12.97.2. Inspect system capability</H4>
<DIV class=openblock>
<DIV class=content>
<DIV class=paragraph>
<P>An application <STRONG class=purple>can</STRONG> inspect whether the system is capable of foveated rendering by chaining an <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrSystemFoveatedRenderingPropertiesVARJO">XrSystemFoveatedRenderingPropertiesVARJO</A> structure to the <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrSystemProperties">XrSystemProperties</A> structure when calling <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#xrGetSystemProperties">xrGetSystemProperties</A>.</P></DIV>
<DIV id=XrSystemFoveatedRenderingPropertiesVARJO class=paragraph>
<P></P></DIV>
<DIV class=listingblock>
<DIV class=content><PRE class="prettyprint highlight"><CODE data-lang="c++">typedef struct XrSystemFoveatedRenderingPropertiesVARJO {
    XrStructureType    type;
    void*              next;
    XrBool32           supportsFoveatedRendering;
} XrSystemFoveatedRenderingPropertiesVARJO;</CODE></PRE></DIV></DIV>
<DIV class=sidebarblock>
<DIV class=content>
<DIV class=title>Member Descriptions</DIV>
<DIV class=ulist>
<UL>
<LI>
<P><CODE>type</CODE> is the <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrStructureType">XrStructureType</A> of this structure.</P></LI>
<LI>
<P><CODE>next</CODE> is <CODE>NULL</CODE> or a pointer to the next structure in a structure chain. No such structures are defined in core OpenXR or this extension.</P></LI>
<LI>
<P><CODE>supportsFoveatedRendering</CODE> is an <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrBool32"><CODE>XrBool32</CODE></A>, indicating if current system is capable of performoning foveated rendering.</P></LI></UL></DIV></DIV></DIV>
<DIV class=paragraph>
<P>The runtime <STRONG class=purple>should</STRONG> return <CODE>XR_TRUE</CODE> for <CODE>supportsFoveatedRendering</CODE> when rendering gaze is available in the system. An application <STRONG class=purple>should</STRONG> avoid using foveated rendering functionality when <CODE>supportsFoveatedRendering</CODE> is <CODE>XR_FALSE</CODE>.</P></DIV>
<DIV class=sidebarblock>
<DIV class=content>
<DIV class=title>Valid Usage (Implicit)</DIV>
<DIV class=ulist>
<UL>
<LI>
<P><A id=VUID-XrSystemFoveatedRenderingPropertiesVARJO-extension-notenabled href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-XrSystemFoveatedRenderingPropertiesVARJO-extension-notenabled"></A>The <CODE><A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XR_VARJO_foveated_rendering">XR_VARJO_foveated_rendering</A></CODE> extension <STRONG class=purple>must</STRONG> be enabled prior to using <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrSystemFoveatedRenderingPropertiesVARJO">XrSystemFoveatedRenderingPropertiesVARJO</A></P></LI>
<LI>
<P><A id=VUID-XrSystemFoveatedRenderingPropertiesVARJO-type-type href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-XrSystemFoveatedRenderingPropertiesVARJO-type-type"></A><CODE>type</CODE> <STRONG class=purple>must</STRONG> be <CODE>XR_TYPE_SYSTEM_FOVEATED_RENDERING_PROPERTIES_VARJO</CODE></P></LI>
<LI>
<P><A id=VUID-XrSystemFoveatedRenderingPropertiesVARJO-next-next href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-XrSystemFoveatedRenderingPropertiesVARJO-next-next"></A><CODE>next</CODE> <STRONG class=purple>must</STRONG> be <CODE>NULL</CODE> or a valid pointer to the <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#valid-usage-for-structure-pointer-chains">next structure in a structure chain</A></P></LI></UL></DIV></DIV></DIV></DIV></DIV></DIV>
<DIV class=sect3>
<H4 id=_determine_foveated_texture_sizes><A class=anchor href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#_determine_foveated_texture_sizes"></A>12.97.3. Determine foveated texture sizes</H4>
<DIV class=openblock>
<DIV class=content>
<DIV class=paragraph>
<P>Foveated textures <STRONG class=purple>may</STRONG> have different sizes and aspect ratio compared to non-foveated textures. In order to determine recommended foveated texture size, an application <STRONG class=purple>can</STRONG> chain <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrFoveatedViewConfigurationViewVARJO">XrFoveatedViewConfigurationViewVARJO</A> to <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrViewConfigurationView">XrViewConfigurationView</A> and set <CODE>foveatedRenderingActive</CODE> to <CODE>XR_TRUE</CODE>. Since an application using foveated rendering with this extension has to render 4 views, <CODE><A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XR_VARJO_quad_views">XR_VARJO_quad_views</A></CODE> <STRONG class=purple>must</STRONG> be enabled along with this extension when <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrInstance">XrInstance</A> is created.</P></DIV>
<DIV class=paragraph>
<P>First and second views are non foveated views (covering whole field of view of HMD), third (left eye) and fourth (right eye) are foveated e.g. following gaze.</P></DIV>
<DIV id=XrFoveatedViewConfigurationViewVARJO class=paragraph>
<P></P></DIV>
<DIV class=listingblock>
<DIV class=content><PRE class="prettyprint highlight"><CODE data-lang="c++">typedef struct XrFoveatedViewConfigurationViewVARJO {
    XrStructureType    type;
    void*              next;
    XrBool32           foveatedRenderingActive;
} XrFoveatedViewConfigurationViewVARJO;</CODE></PRE></DIV></DIV>
<DIV class=sidebarblock>
<DIV class=content>
<DIV class=title>Member Descriptions</DIV>
<DIV class=ulist>
<UL>
<LI>
<P><CODE>type</CODE> is the <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrStructureType">XrStructureType</A> of this structure.</P></LI>
<LI>
<P><CODE>next</CODE> is <CODE>NULL</CODE> or a pointer to the next structure in a structure chain. No such structures are defined in core OpenXR or this extension.</P></LI>
<LI>
<P><CODE>foveatedRenderingActive</CODE> is an <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrBool32"><CODE>XrBool32</CODE></A>, indicating if the runtime should return foveated view configuration view.</P></LI></UL></DIV></DIV></DIV>
<DIV class=sidebarblock>
<DIV class=content>
<DIV class=title>Valid Usage (Implicit)</DIV>
<DIV class=ulist>
<UL>
<LI>
<P><A id=VUID-XrFoveatedViewConfigurationViewVARJO-extension-notenabled href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-XrFoveatedViewConfigurationViewVARJO-extension-notenabled"></A>The <CODE><A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XR_VARJO_foveated_rendering">XR_VARJO_foveated_rendering</A></CODE> extension <STRONG class=purple>must</STRONG> be enabled prior to using <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrFoveatedViewConfigurationViewVARJO">XrFoveatedViewConfigurationViewVARJO</A></P></LI>
<LI>
<P><A id=VUID-XrFoveatedViewConfigurationViewVARJO-type-type href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-XrFoveatedViewConfigurationViewVARJO-type-type"></A><CODE>type</CODE> <STRONG class=purple>must</STRONG> be <CODE>XR_TYPE_FOVEATED_VIEW_CONFIGURATION_VIEW_VARJO</CODE></P></LI>
<LI>
<P><A id=VUID-XrFoveatedViewConfigurationViewVARJO-next-next href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-XrFoveatedViewConfigurationViewVARJO-next-next"></A><CODE>next</CODE> <STRONG class=purple>must</STRONG> be <CODE>NULL</CODE> or a valid pointer to the <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#valid-usage-for-structure-pointer-chains">next structure in a structure chain</A></P></LI></UL></DIV></DIV></DIV>
<DIV class=paragraph>
<P>For example:</P></DIV>
<DIV class=listingblock>
<DIV class=content><PRE class="prettyprint highlight"><CODE data-lang="c++">XrInstance instance; // previously populated
XrSystemId systemId; // previously populated
XrViewConfigurationType viewConfigType; // Select XR_VIEW_CONFIGURATION_TYPE_PRIMARY_QUAD_VARJO

XrSystemFoveatedRenderingPropertiesVARJO foveatedRenderingProperties{XR_TYPE_SYSTEM_FOVEATED_RENDERING_PROPERTIES_VARJO};
XrSystemProperties systemProperties{XR_TYPE_SYSTEM_PROPERTIES, &amp;foveatedRenderingProperties};
CHK_XR(xrGetSystemProperties(instance, systemId, &amp;systemProperties));

uint32_t viewCount;
CHK_XR(xrEnumerateViewConfigurationViews(instance, systemId, viewConfigType, 0, &amp;viewCount, nullptr));
// Non-foveated rendering views dimensions
std::vector&lt;XrViewConfigurationView&gt; configViews(viewCount, {XR_TYPE_VIEW_CONFIGURATION_VIEW});
CHK_XR(xrEnumerateViewConfigurationViews(instance, systemId, viewConfigType, viewCount, &amp;viewCount, configViews.data()));

// Foveated rendering views dimensions
std::vector&lt;XrViewConfigurationView&gt; foveatedViews;
if (foveatedRenderingProperties.supportsFoveatedRendering &amp;&amp; viewConfigType == XR_VIEW_CONFIGURATION_TYPE_PRIMARY_QUAD_VARJO) {
  std::vector&lt;XrFoveatedViewConfigurationViewVARJO&gt; requestFoveatedConfig{4, {XR_TYPE_FOVEATED_VIEW_CONFIGURATION_VIEW_VARJO, nullptr, XR_TRUE}};
  foveatedViews = std::vector&lt;XrViewConfigurationView&gt;{4, {XR_TYPE_VIEW_CONFIGURATION_VIEW}};
  for (size_t i = 0; i &lt; 4; i++) {
    foveatedViews[i].next = &amp;requestFoveatedConfig[i];
  }
  CHK_XR(xrEnumerateViewConfigurationViews(instance, systemId, viewConfigType, viewCount, &amp;viewCount, foveatedViews.data()));
}</CODE></PRE></DIV></DIV>
<DIV class=exampleblock>
<DIV class=title>Example 2. Note</DIV>
<DIV class=content>
<DIV class=paragraph>
<P>Applications using this extension are encouraged to create 2 sets of swapchains or one big enough set of swapchains and 2 sets of viewports. One set will be used when rendering gaze is not available and other one will be used when foveated rendering and rendering gaze is available. Using foveated textures <STRONG class=purple>may</STRONG> not provide optimal visual quality when rendering gaze is not available.</P></DIV></DIV></DIV></DIV></DIV></DIV>
<DIV class=sect3>
<H4 id=_rendering_gaze_status><A class=anchor href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#_rendering_gaze_status"></A>12.97.4. Rendering gaze status</H4>
<DIV class=paragraph>
<P>Extension defines new reference space type - <CODE>XR_REFERENCE_SPACE_TYPE_COMBINED_EYE_VARJO</CODE> which <STRONG class=purple>should</STRONG> be used to determine whether rendering gaze is available. After calling <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#xrLocateSpace">xrLocateSpace</A>, application <STRONG class=purple>should</STRONG> inspect <CODE>XR_SPACE_LOCATION_ORIENTATION_TRACKED_BIT</CODE> bit. If it&#8217;s set, rendering gaze is available otherwise not.</P></DIV>
<DIV class=listingblock>
<DIV class=content><PRE class="prettyprint highlight"><CODE data-lang="c++">XrSession session; // previously populated

// Create needed spaces
XrSpace viewSpace;
XrReferenceSpaceCreateInfo createViewSpaceInfo{XR_TYPE_REFERENCE_SPACE_CREATE_INFO};
createViewSpaceInfo.referenceSpaceType = XR_REFERENCE_SPACE_TYPE_VIEW;
createViewSpaceInfo.poseInReferenceSpace.orientation.w = 1.0f;
CHK_XR(xrCreateReferenceSpace(session, &amp;createViewSpaceInfo, &amp;viewSpace));

XrSpace renderGazeSpace;
XrReferenceSpaceCreateInfo createReferenceSpaceInfo{XR_TYPE_REFERENCE_SPACE_CREATE_INFO};
createReferenceSpaceInfo.referenceSpaceType = XR_REFERENCE_SPACE_TYPE_COMBINED_EYE_VARJO;
createReferenceSpaceInfo.poseInReferenceSpace.orientation.w = 1.0f;
CHK_XR(xrCreateReferenceSpace(session, &amp;createReferenceSpaceInfo, &amp;renderGazeSpace));

// ...
// in frame loop
// ...

XrFrameState frameState; // previously populated by xrWaitFrame

// Query rendering gaze status
XrSpaceLocation renderGazeLocation{XR_TYPE_SPACE_LOCATION};
CHK_XR(xrLocateSpace(renderGazeSpace, viewSpace, frameState.predictedDisplayTime, &amp;renderGazeLocation));

const bool foveationActive = (renderGazeLocation.locationFlags &amp; XR_SPACE_LOCATION_ORIENTATION_TRACKED_BIT) != 0;

if (foveationActive) {
  // Rendering gaze is available
} else {
  // Rendering gaze is not available
}</CODE></PRE></DIV></DIV></DIV>
<DIV class=sect3>
<H4 id=_request_foveated_field_of_view><A class=anchor href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#_request_foveated_field_of_view"></A>12.97.5. Request foveated field of view</H4>
<DIV class=openblock>
<DIV class=content>
<DIV class=paragraph>
<P>For each frame, the application indicates if the runtime will return foveated or non-foveated field of view. This is done by chaining <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrViewLocateFoveatedRenderingVARJO">XrViewLocateFoveatedRenderingVARJO</A> to <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrViewLocateInfo">XrViewLocateInfo</A>.</P></DIV>
<DIV id=XrViewLocateFoveatedRenderingVARJO class=paragraph>
<P></P></DIV>
<DIV class=listingblock>
<DIV class=content><PRE class="prettyprint highlight"><CODE data-lang="c++">typedef struct XrViewLocateFoveatedRenderingVARJO {
    XrStructureType    type;
    const void*        next;
    XrBool32           foveatedRenderingActive;
} XrViewLocateFoveatedRenderingVARJO;</CODE></PRE></DIV></DIV>
<DIV class=sidebarblock>
<DIV class=content>
<DIV class=title>Member Descriptions</DIV>
<DIV class=ulist>
<UL>
<LI>
<P><CODE>type</CODE> is the <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrStructureType">XrStructureType</A> of this structure.</P></LI>
<LI>
<P><CODE>next</CODE> is <CODE>NULL</CODE> or a pointer to the next structure in a structure chain. No such structures are defined in core OpenXR or this extension.</P></LI>
<LI>
<P><CODE>foveatedRenderingActive</CODE> is an <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrBool32"><CODE>XrBool32</CODE></A>, indicating if runtime should return foveated FoV.</P></LI></UL></DIV></DIV></DIV>
<DIV class=paragraph>
<P>The runtime <STRONG class=purple>must</STRONG> return foveated field of view when <CODE>foveatedRenderingActive</CODE> is <CODE>XR_TRUE</CODE>.</P></DIV>
<DIV class=sidebarblock>
<DIV class=content>
<DIV class=title>Valid Usage (Implicit)</DIV>
<DIV class=ulist>
<UL>
<LI>
<P><A id=VUID-XrViewLocateFoveatedRenderingVARJO-extension-notenabled href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-XrViewLocateFoveatedRenderingVARJO-extension-notenabled"></A>The <CODE><A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XR_VARJO_foveated_rendering">XR_VARJO_foveated_rendering</A></CODE> extension <STRONG class=purple>must</STRONG> be enabled prior to using <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrViewLocateFoveatedRenderingVARJO">XrViewLocateFoveatedRenderingVARJO</A></P></LI>
<LI>
<P><A id=VUID-XrViewLocateFoveatedRenderingVARJO-type-type href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-XrViewLocateFoveatedRenderingVARJO-type-type"></A><CODE>type</CODE> <STRONG class=purple>must</STRONG> be <CODE>XR_TYPE_VIEW_LOCATE_FOVEATED_RENDERING_VARJO</CODE></P></LI>
<LI>
<P><A id=VUID-XrViewLocateFoveatedRenderingVARJO-next-next href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-XrViewLocateFoveatedRenderingVARJO-next-next"></A><CODE>next</CODE> <STRONG class=purple>must</STRONG> be <CODE>NULL</CODE> or a valid pointer to the <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#valid-usage-for-structure-pointer-chains">next structure in a structure chain</A></P></LI></UL></DIV></DIV></DIV></DIV></DIV>
<DIV class=listingblock>
<DIV class=content><PRE class="prettyprint highlight"><CODE data-lang="c++">// ...
// in frame loop
// ...

XrSession session; // previously populated
XrSpace appSpace; // previously populated
XrFrameState frameState; // previously populated by xrWaitFrame
std::vector&lt;XrView&gt; views; // previously populated/resized to the correct size
bool foveationActive; // previously populated, as in the previous example

XrViewState viewState{XR_TYPE_VIEW_STATE};
uint32_t viewCapacityInput = static_cast&lt;uint32_t&gt;(views.size());
uint32_t viewCountOutput;
XrViewLocateInfo viewLocateInfo{XR_TYPE_VIEW_LOCATE_INFO};
viewLocateInfo.displayTime = frameState.predictedDisplayTime;
viewLocateInfo.space = appSpace;
XrViewLocateFoveatedRenderingVARJO viewLocateFoveatedRendering{XR_TYPE_VIEW_LOCATE_FOVEATED_RENDERING_VARJO};
viewLocateFoveatedRendering.foveatedRenderingActive = foveationActive;
viewLocateInfo.next = &amp;viewLocateFoveatedRendering;

CHK_XR(xrLocateViews(session, &amp;viewLocateInfo, &amp;viewState, viewCapacityInput, &amp;viewCountOutput, views.data()));</CODE></PRE></DIV></DIV>
<DIV class=paragraph>
<P><STRONG>New Structures</STRONG></P></DIV>
<DIV class=ulist>
<UL>
<LI>
<P><A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrViewLocateFoveatedRenderingVARJO">XrViewLocateFoveatedRenderingVARJO</A></P></LI>
<LI>
<P><A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrFoveatedViewConfigurationViewVARJO">XrFoveatedViewConfigurationViewVARJO</A></P></LI>
<LI>
<P><A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrSystemFoveatedRenderingPropertiesVARJO">XrSystemFoveatedRenderingPropertiesVARJO</A></P></LI></UL></DIV>
<DIV class=paragraph>
<P><STRONG>New Enum Constants</STRONG></P></DIV>
<DIV class=paragraph>
<P><A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrStructureType">XrStructureType</A> enumeration is extended with:</P></DIV>
<DIV class=ulist>
<UL>
<LI>
<P><CODE>XR_TYPE_VIEW_LOCATE_FOVEATED_RENDERING_VARJO</CODE></P></LI>
<LI>
<P><CODE>XR_TYPE_FOVEATED_VIEW_CONFIGURATION_VIEW_VARJO</CODE></P></LI>
<LI>
<P><CODE>XR_TYPE_SYSTEM_FOVEATED_RENDERING_PROPERTIES_VARJO</CODE></P></LI></UL></DIV>
<DIV class=paragraph>
<P><A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrReferenceSpaceType">XrReferenceSpaceType</A> enumeration is extended with:</P></DIV>
<DIV class=ulist>
<UL>
<LI>
<P><CODE>XR_REFERENCE_SPACE_TYPE_COMBINED_EYE_VARJO</CODE></P></LI></UL></DIV>
<DIV class=paragraph>
<P><STRONG>Version History</STRONG></P></DIV>
<DIV class=ulist>
<UL>
<LI>
<P>Revision 1, 2020-12-16 (Sergiy Dubovik)</P>
<DIV class=ulist>
<UL>
<LI>
<P>Initial extension description</P></LI></UL></DIV></LI>
<LI>
<P>Revision 2, 2021-04-13 (Ryan Pavlik, Collabora, and Sergiy Dubovik)</P>
<DIV class=ulist>
<UL>
<LI>
<P>Update sample code so it is buildable</P></LI></UL></DIV></LI></UL></DIV></DIV></DIV>
<DIV class=sect2>