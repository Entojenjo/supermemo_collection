<strong><font color="blue"> : </font></strong><H4 id=_api_layer_extensions><A class=anchor href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#_api_layer_extensions"></A>2.7.4. API Layer Extensions</H4>
<DIV class=paragraph>
<P>API layers <STRONG class=purple>may</STRONG> implement OpenXR functions that may or may not be supported by the underlying runtime. In order to expose these new features, the API layer must expose this functionality in the form of an OpenXR <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#extensions">extension</A>. It <STRONG class=purple>must</STRONG> not expose new OpenXR functions without an associated extension.</P></DIV>
<DIV class=paragraph>
<P>For example, an OpenXR API-logging API layer might expose an API function to allow the application to turn logging on for only a portion of its execution. Since new functions <STRONG class=purple>must</STRONG> be exposed through an extension, the vendor has created an extension called <CODE>XR_ACME_logging_on_off</CODE> to contain these new functions. The application <STRONG class=purple>should</STRONG> query if the API layer supports the extension and then, only if it exists, enable both the extension and the API layer by name during <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#xrCreateInstance">xrCreateInstance</A>.</P></DIV>
<DIV class=paragraph>
<P>To find out what extensions an API layer supports, an application <STRONG class=purple>must</STRONG> first verify that the API layer exists on the current system by calling <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#xrEnumerateApiLayerProperties">xrEnumerateApiLayerProperties</A>. After verifying an API layer of interest exists, the application then <STRONG class=purple>should</STRONG> call <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#xrEnumerateInstanceExtensionProperties">xrEnumerateInstanceExtensionProperties</A> and provide the API layer name as the first parameter. This will return the list of extensions implemented internally in that API layer.</P></DIV></DIV>
<DIV class=sect3>
<H4 id=type-aliasing><A class=anchor href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#type-aliasing"></A>2.7.5. Type Aliasing</H4>
<DIV class=paragraph>
<P>Type aliasing refers to the situation in which the actual type of a element does not match the declared type. Some C and C++ compilers can be configured to assume that the actual type matches the declared type, and may be so configured by default at common optimization levels. Without this, otherwise undefined behavior may occur. This compiler feature is typically referred to as "strict aliasing," and it can usually be enabled or disabled via compiler options. The OpenXR specification does not support strict aliasing, as there are some cases in which an application intentionally provides a struct with a type that differs from the declared type. For example, <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrFrameEndInfo">XrFrameEndInfo</A>::<CODE>layers</CODE> is an array of type <CODE>const</CODE> <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrCompositionLayerBaseHeader">XrCompositionLayerBaseHeader</A> <CODE>*</CODE> <CODE>const</CODE>. However, the array <STRONG class=purple>must</STRONG> be of one of the specific layer types, such as <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrCompositionLayerQuad">XrCompositionLayerQuad</A>. Similarly, <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#xrEnumerateSwapchainImages">xrEnumerateSwapchainImages</A> accepts an array of <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrSwapchainImageBaseHeader">XrSwapchainImageBaseHeader</A>, whereas the actual type passed <STRONG class=purple>must</STRONG> be an array of a type such as <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrSwapchainImageVulkanKHR">XrSwapchainImageVulkanKHR</A>. For OpenXR to work correctly, the compiler <STRONG class=purple>must</STRONG> support the type aliasing described here.</P></DIV>
<DIV class=openblock>
<DIV class=content>
<DIV id=XR_MAY_ALIAS class=paragraph>
<P></P></DIV>
<DIV class=listingblock>
<DIV class=content><PRE class="prettyprint highlight"><CODE data-lang="c++">#if !defined(XR_MAY_ALIAS)
#if defined(__clang__) || (defined(__GNUC__) &amp;&amp; (__GNUC__ &gt; 4))
#define XR_MAY_ALIAS __attribute__((__may_alias__))
#else
#define XR_MAY_ALIAS
#endif
#endif</CODE></PRE></DIV></DIV>
<DIV class=paragraph>
<P>As a convenience, some types and pointers that are known at specification time to alias values of different types have been annotated with the <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XR_MAY_ALIAS">XR_MAY_ALIAS</A> definition. If this macro is not defined before including OpenXR headers, and a new enough Clang or GCC compiler is used, it will be defined to the compiler-specific attribute annotation to inform these compilers that those pointers may alias. However, there is no guarantee that all aliasing types or pointers have been correctly marked with this macro, so thorough testing is still recommended if you choose (at your own risk) to permit your compiler to perform type-based aliasing analysis.</P></DIV></DIV></DIV></DIV>
<DIV class=sect3>
<H4 id=valid-usage><A class=anchor href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#valid-usage"></A>2.7.6. Valid Usage</H4>
<DIV class=paragraph>
<P>Valid usage defines a set of conditions which <STRONG class=purple>must</STRONG> be met in order to achieve well-defined run-time behavior in an application. These conditions depend only on API state, and the parameters or objects whose usage is constrained by the condition.</P></DIV>
<DIV class=paragraph>
<P>Some valid usage conditions have dependencies on runtime limits or feature availability. It is possible to validate these conditions against the API&#8217;s minimum or maximum supported values for these limits and features, or some subset of other known values.</P></DIV>
<DIV class=paragraph>
<P>Valid usage conditions <STRONG class=purple>should</STRONG> apply to a function or structure where complete information about the condition would be known during execution of an application. This is such that a validation API layer or linter <STRONG class=purple>can</STRONG> be written directly against these statements at the point they are specified.</P></DIV></DIV>
<DIV class=sect3>
<H4 id=implicit-valid-usage><A class=anchor href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#implicit-valid-usage"></A>2.7.7. Implicit Valid Usage</H4>
<DIV class=paragraph>
<P>Some valid usage conditions apply to all functions and structures in the API, unless explicitly denoted otherwise for a specific function or structure. These conditions are considered implicit. Implicit valid usage conditions are described in detail below.</P></DIV>
<DIV class=sect4>