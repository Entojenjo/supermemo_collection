<strong><font color="blue"> : </font></strong><H3 id=_action_overview><A class=anchor href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#_action_overview"></A>11.1. Action Overview</H3>
<DIV class=paragraph>
<P>OpenXR applications communicate with input devices using XrActions. Actions are created at initialization time and later used to request input device state, create action spaces, or control haptic events. Input action handles represent 'actions' that the application is interested in obtaining the state of, not direct input device hardware. For example, instead of the application directly querying the state of the A button when interacting with a menu, an OpenXR application instead creates a <CODE>menu_select</CODE> action at startup then asks OpenXR for the state of the action.</P></DIV>
<DIV class=paragraph>
<P>The application recommends that the action be assigned to a specific input source on the input device for a known <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#semantic-path-interaction-profiles">interaction profile</A>, but runtimes have the ability to choose a different control depending on user preference, input device availability, or any other reason. This abstraction ensures that applications can run on a wide variety of input hardware and maximize user accessibility.</P></DIV>
<DIV class=paragraph>
<P>Example usage:</P></DIV>
<DIV class=listingblock>
<DIV class=content><PRE class="prettyprint highlight"><CODE data-lang="C++">XrInstance instance; // previously initialized
XrSession session; // previously initialized

// Create an action set
XrActionSetCreateInfo actionSetInfo{XR_TYPE_ACTION_SET_CREATE_INFO};
strcpy(actionSetInfo.actionSetName, "gameplay");
strcpy(actionSetInfo.localizedActionSetName, "Gameplay");
actionSetInfo.priority = 0;
XrActionSet inGameActionSet;
CHK_XR(xrCreateActionSet(instance, &amp;actionSetInfo, &amp;inGameActionSet));

// create a "teleport" input action
XrActionCreateInfo actioninfo{XR_TYPE_ACTION_CREATE_INFO};
strcpy(actioninfo.actionName, "teleport");
actioninfo.actionType = XR_ACTION_TYPE_BOOLEAN_INPUT;
strcpy(actioninfo.localizedActionName, "Teleport");
XrAction teleportAction;
CHK_XR(xrCreateAction(inGameActionSet, &amp;actioninfo, &amp;teleportAction));

// create a "player_hit" output action
XrActionCreateInfo hapticsactioninfo{XR_TYPE_ACTION_CREATE_INFO};
strcpy(hapticsactioninfo.actionName, "player_hit");
hapticsactioninfo.actionType = XR_ACTION_TYPE_VIBRATION_OUTPUT;
strcpy(hapticsactioninfo.localizedActionName, "Player hit");
XrAction hapticsAction;
CHK_XR(xrCreateAction(inGameActionSet, &amp;hapticsactioninfo, &amp;hapticsAction));

XrPath triggerClickPath, hapticPath;
CHK_XR(xrStringToPath(instance, "/user/hand/right/input/trigger/click", &amp;triggerClickPath));
CHK_XR(xrStringToPath(instance, "/user/hand/right/output/haptic", &amp;hapticPath))

XrPath interactionProfilePath;
CHK_XR(xrStringToPath(instance, "/interaction_profiles/vendor_x/profile_x", &amp;interactionProfilePath));

XrActionSuggestedBinding bindings[2];
bindings[0].action = teleportAction;
bindings[0].binding = triggerClickPath;
bindings[1].action = hapticsAction;
bindings[1].binding = hapticPath;

XrInteractionProfileSuggestedBinding suggestedBindings{XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING};
suggestedBindings.interactionProfile = interactionProfilePath;
suggestedBindings.suggestedBindings = bindings;
suggestedBindings.countSuggestedBindings = 2;
CHK_XR(xrSuggestInteractionProfileBindings(instance, &amp;suggestedBindings));

XrSessionActionSetsAttachInfo attachInfo{XR_TYPE_SESSION_ACTION_SETS_ATTACH_INFO};
attachInfo.countActionSets = 1;
attachInfo.actionSets = &amp;inGameActionSet;
CHK_XR(xrAttachSessionActionSets(session, &amp;attachInfo));

// application main loop
while (1)
{
    // sync action data
    XrActiveActionSet activeActionSet{inGameActionSet, XR_NULL_PATH};
    XrActionsSyncInfo syncInfo{XR_TYPE_ACTIONS_SYNC_INFO};
    syncInfo.countActiveActionSets = 1;
    syncInfo.activeActionSets = &amp;activeActionSet;
    CHK_XR(xrSyncActions(session, &amp;syncInfo));

    // query input action state
    XrActionStateBoolean teleportState{XR_TYPE_ACTION_STATE_BOOLEAN};
    XrActionStateGetInfo getInfo{XR_TYPE_ACTION_STATE_GET_INFO};
    getInfo.action = teleportAction;
    CHK_XR(xrGetActionStateBoolean(session, &amp;getInfo, &amp;teleportState));

    if (teleportState.changedSinceLastSync &amp;&amp; teleportState.currentState)
    {
        // fire haptics using output action
        XrHapticVibration vibration{XR_TYPE_HAPTIC_VIBRATION};
        vibration.amplitude = 0.5;
        vibration.duration = 300;
        vibration.frequency = 3000;
        XrHapticActionInfo hapticActionInfo{XR_TYPE_HAPTIC_ACTION_INFO};
        hapticActionInfo.action = hapticsAction;
        CHK_XR(xrApplyHapticFeedback(session, &amp;hapticActionInfo, (const XrHapticBaseHeader*)&amp;vibration));
    }
}</CODE></PRE></DIV></DIV></DIV>
<DIV class=sect2>
<H3 id=input-action-creation><A class=anchor href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#input-action-creation"></A>11.2. Action Sets</H3>
<DIV class=openblock>
<DIV class=content>
<DIV id=XrActionSet class=paragraph>
<P></P></DIV>
<DIV class=listingblock>
<DIV class=content><PRE class="prettyprint highlight"><CODE data-lang="c++">XR_DEFINE_HANDLE(XrActionSet)</CODE></PRE></DIV></DIV>
<DIV class=paragraph>
<P>Action sets are application-defined collections of actions. They are attached to a given <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrSession">XrSession</A> with a <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#xrAttachSessionActionSets">xrAttachSessionActionSets</A> call. They are enabled or disabled by the application via <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#xrSyncActions">xrSyncActions</A> depending on the current application context. For example, a game may have one set of actions that apply to controlling a character and another set for navigating a menu system. When these actions are grouped into two <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrActionSet">XrActionSet</A> handles they can be selectively enabled and disabled using a single function call.</P></DIV>
<DIV class=paragraph>
<P>Actions are passed a handle to their <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrActionSet">XrActionSet</A> when they are created.</P></DIV>
<DIV class=paragraph>
<P>Action sets are created by calling <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#xrCreateActionSet">xrCreateActionSet</A>:</P></DIV></DIV></DIV>
<DIV class=openblock>
<DIV class=content>
<DIV class=paragraph>
<P>The <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#xrCreateActionSet">xrCreateActionSet</A> function is defined as:</P></DIV>
<DIV id=xrCreateActionSet class=paragraph>
<P></P></DIV>
<DIV class=listingblock>
<DIV class=content><PRE class="prettyprint highlight"><CODE data-lang="c++">XrResult xrCreateActionSet(
    XrInstance                                  instance,
    const XrActionSetCreateInfo*                createInfo,
    XrActionSet*                                actionSet);</CODE></PRE></DIV></DIV>
<DIV class=sidebarblock>
<DIV class=content>
<DIV class=title>Parameter Descriptions</DIV>
<DIV class=ulist>
<UL>
<LI>
<P><CODE>instance</CODE> is a handle to an <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrInstance">XrInstance</A>.</P></LI>
<LI>
<P><CODE>createInfo</CODE> is a pointer to a valid <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrActionSetCreateInfo">XrActionSetCreateInfo</A> structure that defines the action set being created.</P></LI>
<LI>
<P><CODE>actionSet</CODE> is a pointer to an <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrActionSet">XrActionSet</A> where the created action set is returned.</P></LI></UL></DIV></DIV></DIV>
<DIV class=paragraph>
<P>The <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#xrCreateActionSet">xrCreateActionSet</A> function creates an action set and returns a handle to the created action set.</P></DIV>
<DIV class=sidebarblock>
<DIV class=content>
<DIV class=title>Valid Usage (Implicit)</DIV>
<DIV class=ulist>
<UL>
<LI>
<P><A id=VUID-xrCreateActionSet-instance-parameter href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-xrCreateActionSet-instance-parameter"></A><CODE>instance</CODE> <STRONG class=purple>must</STRONG> be a valid <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrInstance">XrInstance</A> handle</P></LI>
<LI>
<P><A id=VUID-xrCreateActionSet-createInfo-parameter href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-xrCreateActionSet-createInfo-parameter"></A><CODE>createInfo</CODE> <STRONG class=purple>must</STRONG> be a pointer to a valid <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrActionSetCreateInfo">XrActionSetCreateInfo</A> structure</P></LI>
<LI>
<P><A id=VUID-xrCreateActionSet-actionSet-parameter href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-xrCreateActionSet-actionSet-parameter"></A><CODE>actionSet</CODE> <STRONG class=purple>must</STRONG> be a pointer to an <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrActionSet">XrActionSet</A> handle</P></LI></UL></DIV></DIV></DIV>
<DIV class=sidebarblock>
<DIV class=content>
<DIV class=title>Return Codes</DIV>
<DIV class=dlist>
<DL>
<DT class=hdlist1><A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#fundamentals-successcodes">Success</A></DT>
<DD>
<DIV class=ulist>
<UL>
<LI>
<P><CODE>XR_SUCCESS</CODE></P></LI></UL></DIV></DD>
<DT class=hdlist1><A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#fundamentals-errorcodes">Failure</A></DT>
<DD>
<DIV class=ulist>
<UL>
<LI>
<P><CODE>XR_ERROR_VALIDATION_FAILURE</CODE></P></LI>
<LI>
<P><CODE>XR_ERROR_RUNTIME_FAILURE</CODE></P></LI>
<LI>
<P><CODE>XR_ERROR_HANDLE_INVALID</CODE></P></LI>
<LI>
<P><CODE>XR_ERROR_INSTANCE_LOST</CODE></P></LI>
<LI>
<P><CODE>XR_ERROR_OUT_OF_MEMORY</CODE></P></LI>
<LI>
<P><CODE>XR_ERROR_LIMIT_REACHED</CODE></P></LI>
<LI>
<P><CODE>XR_ERROR_PATH_FORMAT_INVALID</CODE></P></LI>
<LI>
<P><CODE>XR_ERROR_NAME_INVALID</CODE></P></LI>
<LI>
<P><CODE>XR_ERROR_NAME_DUPLICATED</CODE></P></LI>
<LI>
<P><CODE>XR_ERROR_LOCALIZED_NAME_INVALID</CODE></P></LI>
<LI>
<P><CODE>XR_ERROR_LOCALIZED_NAME_DUPLICATED</CODE></P></LI></UL></DIV></DD></DL></DIV></DIV></DIV></DIV></DIV>
<DIV class=openblock>
<DIV class=content>
<DIV class=paragraph>
<P>The <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrActionSetCreateInfo">XrActionSetCreateInfo</A> structure is defined as:</P></DIV>
<DIV id=XrActionSetCreateInfo class=paragraph>
<P></P></DIV>
<DIV class=listingblock>
<DIV class=content><PRE class="prettyprint highlight"><CODE data-lang="c++">typedef struct XrActionSetCreateInfo {
    XrStructureType    type;
    const void*        next;
    char               actionSetName[XR_MAX_ACTION_SET_NAME_SIZE];
    char               localizedActionSetName[XR_MAX_LOCALIZED_ACTION_SET_NAME_SIZE];
    uint32_t           priority;
} XrActionSetCreateInfo;</CODE></PRE></DIV></DIV>
<DIV class=sidebarblock>
<DIV class=content>
<DIV class=title>Member Descriptions</DIV>
<DIV class=ulist>
<UL>
<LI>
<P><CODE>type</CODE> is the <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrStructureType">XrStructureType</A> of this structure.</P></LI>
<LI>
<P><CODE>next</CODE> is <CODE>NULL</CODE> or a pointer to the next structure in a structure chain. No such structures are defined in core OpenXR.</P></LI>
<LI>
<P><CODE>actionSetName</CODE> is an array containing a <CODE>NULL</CODE> terminated non-empty string with the name of this action set.</P></LI>
<LI>
<P><CODE>localizedActionSetName</CODE> is an array containing a <CODE>NULL</CODE> terminated <CODE>UTF</CODE>-8 string that can be presented to the user as a description of the action set. This string should be presented in the system&#8217;s current active locale.</P></LI>
<LI>
<P><CODE>priority</CODE> defines which action sets' actions are active on a given input source when actions on multiple active action sets are bound to the same input source. Larger priority numbers take precedence over smaller priority numbers.</P></LI></UL></DIV></DIV></DIV>
<DIV class=paragraph>
<P>When multiple actions are bound to the same input source, the <CODE>priority</CODE> of each action set determines which bindings are suppressed. Runtimes <STRONG class=purple>must</STRONG> ignore input sources from action sets with a lower priority number if those specific input sources are also present in active actions within a higher priority action set. If multiple action sets with the same priority are bound to the same input source and that is the highest priority number, runtimes <STRONG class=purple>must</STRONG> process all those bindings at the same time.</P></DIV>
<DIV class=paragraph>
<P>Two actions are considered to be bound to the same input source if they use the same <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#semantic-path-input">identifier and optional location</A> path segments, even if they have different component segments.</P></DIV>
<DIV class=paragraph>
<P>When runtimes are ignoring bindings because of priority, they <STRONG class=purple>must</STRONG> treat the binding to that input source as though they do not exist. That means the <CODE>isActive</CODE> field <STRONG class=purple>must</STRONG> be <CODE>XR_FALSE</CODE> when retrieving action data, and that the runtime <STRONG class=purple>must</STRONG> not provide any visual, haptic, or other feedback related to the binding of that action to that input source. Other actions in the same action set which are bound to input sources that do not collide are not affected and are processed as normal.</P></DIV>
<DIV class=paragraph>
<P>If <CODE>actionSetName</CODE> or <CODE>localizedActionSetName</CODE> are empty strings, the runtime <STRONG class=purple>must</STRONG> return <CODE>XR_ERROR_NAME_INVALID</CODE> or <CODE>XR_ERROR_LOCALIZED_NAME_INVALID</CODE> respectively. If <CODE>actionSetName</CODE> or <CODE>localizedActionSetName</CODE> are duplicates of the corresponding field for any existing action set in the specified instance, the runtime <STRONG class=purple>must</STRONG> return <CODE>XR_ERROR_NAME_DUPLICATED</CODE> or <CODE>XR_ERROR_LOCALIZED_NAME_DUPLICATED</CODE> respectively. If the conflicting action set is destroyed, the conflicting field is no longer considered duplicated. If <CODE>actionSetName</CODE> contains characters which are not allowed in a single level of a <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#well-formed-path-strings">well-formed path string</A>, the runtime <STRONG class=purple>must</STRONG> return <CODE>XR_ERROR_PATH_FORMAT_INVALID</CODE>.</P></DIV>
<DIV class=sidebarblock>
<DIV class=content>
<DIV class=title>Valid Usage (Implicit)</DIV>
<DIV class=ulist>
<UL>
<LI>
<P><A id=VUID-XrActionSetCreateInfo-type-type href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-XrActionSetCreateInfo-type-type"></A><CODE>type</CODE> <STRONG class=purple>must</STRONG> be <CODE>XR_TYPE_ACTION_SET_CREATE_INFO</CODE></P></LI>
<LI>
<P><A id=VUID-XrActionSetCreateInfo-next-next href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-XrActionSetCreateInfo-next-next"></A><CODE>next</CODE> <STRONG class=purple>must</STRONG> be <CODE>NULL</CODE> or a valid pointer to the <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#valid-usage-for-structure-pointer-chains">next structure in a structure chain</A></P></LI>
<LI>
<P><A id=VUID-XrActionSetCreateInfo-actionSetName-parameter href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-XrActionSetCreateInfo-actionSetName-parameter"></A><CODE>actionSetName</CODE> <STRONG class=purple>must</STRONG> be a null-terminated UTF-8 string whose length is less than or equal to <CODE>XR_MAX_ACTION_SET_NAME_SIZE</CODE></P></LI>
<LI>
<P><A id=VUID-XrActionSetCreateInfo-localizedActionSetName-parameter href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-XrActionSetCreateInfo-localizedActionSetName-parameter"></A><CODE>localizedActionSetName</CODE> <STRONG class=purple>must</STRONG> be a null-terminated UTF-8 string whose length is less than or equal to <CODE>XR_MAX_LOCALIZED_ACTION_SET_NAME_SIZE</CODE></P></LI></UL></DIV></DIV></DIV></DIV></DIV>
<DIV class=openblock>
<DIV class=content>
<DIV class=paragraph>
<P>The <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#xrDestroyActionSet">xrDestroyActionSet</A> function is defined as:</P></DIV>
<DIV id=xrDestroyActionSet class=paragraph>
<P></P></DIV>
<DIV class=listingblock>
<DIV class=content><PRE class="prettyprint highlight"><CODE data-lang="c++">XrResult xrDestroyActionSet(
    XrActionSet                                 actionSet);</CODE></PRE></DIV></DIV>
<DIV class=sidebarblock>
<DIV class=content>
<DIV class=title>Parameter Descriptions</DIV>
<DIV class=ulist>
<UL>
<LI>
<P><CODE>actionSet</CODE> is the action set to destroy.</P></LI></UL></DIV></DIV></DIV>
<DIV class=paragraph>
<P>Action set handles <STRONG class=purple>can</STRONG> be destroyed by calling <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#xrDestroyActionSet">xrDestroyActionSet</A>. When an action set handle is destroyed, all handles of actions in that action set are also destroyed.</P></DIV>
<DIV class=paragraph>
<P>The implementation <STRONG class=purple>must</STRONG> not free underlying resources for the action set while there are other valid handles that refer to those resources. The implementation <STRONG class=purple>may</STRONG> release resources for an action set when all of the action spaces for actions in that action set have been destroyed. See <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#spaces-action-spaces-lifetime">Action Spaces Lifetime</A> for details.</P></DIV>
<DIV class=paragraph>
<P>Resources for all action sets in an instance <STRONG class=purple>must</STRONG> be freed when the instance containing those actions sets is destroyed.</P></DIV>
<DIV class=sidebarblock>
<DIV class=content>
<DIV class=title>Valid Usage (Implicit)</DIV>
<DIV class=ulist>
<UL>
<LI>
<P><A id=VUID-xrDestroyActionSet-actionSet-parameter href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-xrDestroyActionSet-actionSet-parameter"></A><CODE>actionSet</CODE> <STRONG class=purple>must</STRONG> be a valid <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrActionSet">XrActionSet</A> handle</P></LI></UL></DIV></DIV></DIV>
<DIV class=sidebarblock>
<DIV class=content>
<DIV class=title>Thread Safety</DIV>
<DIV class=ulist>
<UL>
<LI>
<P>Access to <CODE>actionSet</CODE>, and any child handles, <STRONG class=purple>must</STRONG> be externally synchronized</P></LI></UL></DIV></DIV></DIV>
<DIV class=sidebarblock>
<DIV class=content>
<DIV class=title>Return Codes</DIV>
<DIV class=dlist>
<DL>
<DT class=hdlist1><A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#fundamentals-successcodes">Success</A></DT>
<DD>
<DIV class=ulist>
<UL>
<LI>
<P><CODE>XR_SUCCESS</CODE></P></LI></UL></DIV></DD>
<DT class=hdlist1><A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#fundamentals-errorcodes">Failure</A></DT>
<DD>
<DIV class=ulist>
<UL>
<LI>
<P><CODE>XR_ERROR_HANDLE_INVALID</CODE></P></LI></UL></DIV></DD></DL></DIV></DIV></DIV></DIV></DIV></DIV>
<DIV class=sect2>
<H3 id=_creating_actions><A class=anchor href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#_creating_actions"></A>11.3. Creating Actions</H3>
<DIV class=openblock>
<DIV class=content>
<DIV id=XrAction class=paragraph>
<P></P></DIV>
<DIV class=listingblock>
<DIV class=content><PRE class="prettyprint highlight"><CODE data-lang="c++">XR_DEFINE_HANDLE(XrAction)</CODE></PRE></DIV></DIV>
<DIV class=paragraph>
<P>Action handles are used to refer to individual actions when retrieving action data, creating action spaces, or sending haptic events.</P></DIV></DIV></DIV>
<DIV class=openblock>
<DIV class=content>
<DIV class=paragraph>
<P>The <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#xrCreateAction">xrCreateAction</A> function is defined as:</P></DIV>
<DIV id=xrCreateAction class=paragraph>
<P></P></DIV>
<DIV class=listingblock>
<DIV class=content><PRE class="prettyprint highlight"><CODE data-lang="c++">XrResult xrCreateAction(
    XrActionSet                                 actionSet,
    const XrActionCreateInfo*                   createInfo,
    XrAction*                                   action);</CODE></PRE></DIV></DIV>
<DIV class=sidebarblock>
<DIV class=content>
<DIV class=title>Parameter Descriptions</DIV>
<DIV class=ulist>
<UL>
<LI>
<P><CODE>actionSet</CODE> is a handle to an <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrActionSet">XrActionSet</A>.</P></LI>
<LI>
<P><CODE>createInfo</CODE> is a pointer to a valid <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrActionCreateInfo">XrActionCreateInfo</A> structure that defines the action being created.</P></LI>
<LI>
<P><CODE>action</CODE> is a pointer to an <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrAction">XrAction</A> where the created action is returned.</P></LI></UL></DIV></DIV></DIV>
<DIV class=paragraph>
<P><A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#xrCreateAction">xrCreateAction</A> creates an action and returns its handle.</P></DIV>
<DIV class=paragraph>
<P>If <CODE>actionSet</CODE> has been included in a call to <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#xrAttachSessionActionSets">xrAttachSessionActionSets</A>, the implementation <STRONG class=purple>must</STRONG> return <CODE>XR_ERROR_ACTIONSETS_ALREADY_ATTACHED</CODE>.</P></DIV>
<DIV class=sidebarblock>
<DIV class=content>
<DIV class=title>Valid Usage (Implicit)</DIV>
<DIV class=ulist>
<UL>
<LI>
<P><A id=VUID-xrCreateAction-actionSet-parameter href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-xrCreateAction-actionSet-parameter"></A><CODE>actionSet</CODE> <STRONG class=purple>must</STRONG> be a valid <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrActionSet">XrActionSet</A> handle</P></LI>
<LI>
<P><A id=VUID-xrCreateAction-createInfo-parameter href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-xrCreateAction-createInfo-parameter"></A><CODE>createInfo</CODE> <STRONG class=purple>must</STRONG> be a pointer to a valid <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrActionCreateInfo">XrActionCreateInfo</A> structure</P></LI>
<LI>
<P><A id=VUID-xrCreateAction-action-parameter href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-xrCreateAction-action-parameter"></A><CODE>action</CODE> <STRONG class=purple>must</STRONG> be a pointer to an <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrAction">XrAction</A> handle</P></LI></UL></DIV></DIV></DIV>
<DIV class=sidebarblock>
<DIV class=content>
<DIV class=title>Return Codes</DIV>
<DIV class=dlist>
<DL>
<DT class=hdlist1><A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#fundamentals-successcodes">Success</A></DT>
<DD>
<DIV class=ulist>
<UL>
<LI>
<P><CODE>XR_SUCCESS</CODE></P></LI></UL></DIV></DD>
<DT class=hdlist1><A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#fundamentals-errorcodes">Failure</A></DT>
<DD>
<DIV class=ulist>
<UL>
<LI>
<P><CODE>XR_ERROR_VALIDATION_FAILURE</CODE></P></LI>
<LI>
<P><CODE>XR_ERROR_RUNTIME_FAILURE</CODE></P></LI>
<LI>
<P><CODE>XR_ERROR_HANDLE_INVALID</CODE></P></LI>
<LI>
<P><CODE>XR_ERROR_INSTANCE_LOST</CODE></P></LI>
<LI>
<P><CODE>XR_ERROR_OUT_OF_MEMORY</CODE></P></LI>
<LI>
<P><CODE>XR_ERROR_LIMIT_REACHED</CODE></P></LI>
<LI>
<P><CODE>XR_ERROR_PATH_UNSUPPORTED</CODE></P></LI>
<LI>
<P><CODE>XR_ERROR_PATH_INVALID</CODE></P></LI>
<LI>
<P><CODE>XR_ERROR_PATH_FORMAT_INVALID</CODE></P></LI>
<LI>
<P><CODE>XR_ERROR_NAME_INVALID</CODE></P></LI>
<LI>
<P><CODE>XR_ERROR_NAME_DUPLICATED</CODE></P></LI>
<LI>
<P><CODE>XR_ERROR_LOCALIZED_NAME_INVALID</CODE></P></LI>
<LI>
<P><CODE>XR_ERROR_LOCALIZED_NAME_DUPLICATED</CODE></P></LI>
<LI>
<P><CODE>XR_ERROR_ACTIONSETS_ALREADY_ATTACHED</CODE></P></LI></UL></DIV></DD></DL></DIV></DIV></DIV></DIV></DIV>
<DIV class=openblock>
<DIV class=content>
<DIV class=paragraph>
<P>The <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrActionCreateInfo">XrActionCreateInfo</A> structure is defined as:</P></DIV>
<DIV id=XrActionCreateInfo class=paragraph>
<P></P></DIV>
<DIV class=listingblock>
<DIV class=content><PRE class="prettyprint highlight"><CODE data-lang="c++">typedef struct XrActionCreateInfo {
    XrStructureType    type;
    const void*        next;
    char               actionName[XR_MAX_ACTION_NAME_SIZE];
    XrActionType       actionType;
    uint32_t           countSubactionPaths;
    const XrPath*      subactionPaths;
    char               localizedActionName[XR_MAX_LOCALIZED_ACTION_NAME_SIZE];
} XrActionCreateInfo;</CODE></PRE></DIV></DIV>
<DIV class=sidebarblock>
<DIV class=content>
<DIV class=title>Member Descriptions</DIV>
<DIV class=ulist>
<UL>
<LI>
<P><CODE>type</CODE> is the <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrStructureType">XrStructureType</A> of this structure.</P></LI>
<LI>
<P><CODE>next</CODE> is <CODE>NULL</CODE> or a pointer to the next structure in a structure chain. No such structures are defined in core OpenXR.</P></LI>
<LI>
<P><CODE>actionName</CODE> is an array containing a <CODE>NULL</CODE> terminated string with the name of this action.</P></LI>
<LI>
<P><CODE>actionType</CODE> is the <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrActionType">XrActionType</A> of the action to be created.</P></LI>
<LI>
<P><CODE>countSubactionPaths</CODE> is the number of elements in the <CODE>subactionPaths</CODE> array. If <CODE>subactionPaths</CODE> is NULL, this parameter must be 0.</P></LI>
<LI>
<P><CODE>subactionPaths</CODE> is an array of <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrPath"><CODE>XrPath</CODE></A> or <CODE>NULL</CODE>. If this array is specified, it contains one or more subaction paths that the application intends to query action state for.</P></LI>
<LI>
<P><CODE>localizedActionName</CODE> is an array containing a <CODE>NULL</CODE> terminated <CODE>UTF</CODE>-8 string that can be presented to the user as a description of the action. This string should be in the system&#8217;s current active locale.</P></LI></UL></DIV></DIV></DIV>
<DIV class=paragraph>
<P>Subaction paths are a mechanism that enables applications to use the same action name and handle on multiple devices. Applications can query action state using subaction paths that differentiate data coming from each device. This allows the runtime to group logically equivalent actions together in system UI. For instance, an application could create a single <CODE>pick_up</CODE> action with the <EM>/user/hand/left</EM> and <EM>/user/hand/right</EM> subaction paths and use the subaction paths to independently query the state of <CODE>pick_up_with_left_hand</CODE> and <CODE>pick_up_with_right_hand</CODE>.</P></DIV>
<DIV class=paragraph>
<P>Applications <STRONG class=purple>can</STRONG> create actions with or without the <CODE>subactionPaths</CODE> set to a list of paths. If this list of paths is omitted (i.e. <CODE>subactionPaths</CODE> is set to <CODE>NULL</CODE>, and <CODE>countSubactionPaths</CODE> is set to <CODE>0</CODE>), the application is opting out of filtering action results by subaction paths and any call to get action data must also omit subaction paths.</P></DIV>
<DIV class=paragraph>
<P>If <CODE>subactionPaths</CODE> is specified and any of the following conditions are not satisfied, the runtime <STRONG class=purple>must</STRONG> return <CODE>XR_ERROR_PATH_UNSUPPORTED</CODE>:</P></DIV>
<DIV class=ulist>
<UL>
<LI>
<P>Each path provided is one of:</P>
<DIV class=ulist>
<UL>
<LI>
<P><EM>/user/head</EM></P></LI>
<LI>
<P><EM>/user/hand/left</EM></P></LI>
<LI>
<P><EM>/user/hand/right</EM></P></LI>
<LI>
<P><EM>/user/gamepad</EM></P></LI></UL></DIV></LI>
<LI>
<P>No path appears in the list more than once</P></LI></UL></DIV>
<DIV class=paragraph>
<P>Extensions <STRONG class=purple>may</STRONG> append additional top level user paths to the above list.</P></DIV>
<DIV class="admonitionblock note">
<TABLE>
<TBODY>
<TR>
<TD class=icon><I title=Note class="fa icon-note"></I></TD>
<TD class=content>
<DIV class=title>Note</DIV>
<DIV class=paragraph>
<P>Earlier revisions of the spec mentioned <EM>/user</EM> but it could not be implemented as specified and was removed as errata.</P></DIV></TD></TR></TBODY></TABLE></DIV>
<DIV class=paragraph>
<P>The runtime <STRONG class=purple>must</STRONG> return <CODE>XR_ERROR_PATH_UNSUPPORTED</CODE> in the following circumstances:</P></DIV>
<DIV class=ulist>
<UL>
<LI>
<P>The application specified subaction paths at action creation and the application called <CODE>xrGetActionState*</CODE> or a haptic function with an empty subaction path array.</P></LI>
<LI>
<P>The application called <CODE>xrGetActionState*</CODE> or a haptic function with a subaction path that was not specified when the action was created.</P></LI></UL></DIV>
<DIV class=paragraph>
<P>If <CODE>actionName</CODE> or <CODE>localizedActionName</CODE> are empty strings, the runtime <STRONG class=purple>must</STRONG> return <CODE>XR_ERROR_NAME_INVALID</CODE> or <CODE>XR_ERROR_LOCALIZED_NAME_INVALID</CODE> respectively. If <CODE>actionName</CODE> or <CODE>localizedActionName</CODE> are duplicates of the corresponding field for any existing action in the specified action set, the runtime <STRONG class=purple>must</STRONG> return <CODE>XR_ERROR_NAME_DUPLICATED</CODE> or <CODE>XR_ERROR_LOCALIZED_NAME_DUPLICATED</CODE> respectively. If the conflicting action is destroyed, the conflicting field is no longer considered duplicated. If <CODE>actionName</CODE> contains characters which are not allowed in a single level of a <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#well-formed-path-strings">well-formed path string</A>, the runtime <STRONG class=purple>must</STRONG> return <CODE>XR_ERROR_PATH_FORMAT_INVALID</CODE>.</P></DIV>
<DIV class=sidebarblock>
<DIV class=content>
<DIV class=title>Valid Usage (Implicit)</DIV>
<DIV class=ulist>
<UL>
<LI>
<P><A id=VUID-XrActionCreateInfo-type-type href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-XrActionCreateInfo-type-type"></A><CODE>type</CODE> <STRONG class=purple>must</STRONG> be <CODE>XR_TYPE_ACTION_CREATE_INFO</CODE></P></LI>
<LI>
<P><A id=VUID-XrActionCreateInfo-next-next href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-XrActionCreateInfo-next-next"></A><CODE>next</CODE> <STRONG class=purple>must</STRONG> be <CODE>NULL</CODE> or a valid pointer to the <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#valid-usage-for-structure-pointer-chains">next structure in a structure chain</A></P></LI>
<LI>
<P><A id=VUID-XrActionCreateInfo-actionName-parameter href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-XrActionCreateInfo-actionName-parameter"></A><CODE>actionName</CODE> <STRONG class=purple>must</STRONG> be a null-terminated UTF-8 string whose length is less than or equal to <CODE>XR_MAX_ACTION_NAME_SIZE</CODE></P></LI>
<LI>
<P><A id=VUID-XrActionCreateInfo-actionType-parameter href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-XrActionCreateInfo-actionType-parameter"></A><CODE>actionType</CODE> <STRONG class=purple>must</STRONG> be a valid <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrActionType">XrActionType</A> value</P></LI>
<LI>
<P><A id=VUID-XrActionCreateInfo-subactionPaths-parameter href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-XrActionCreateInfo-subactionPaths-parameter"></A>If <CODE>countSubactionPaths</CODE> is not <CODE>0</CODE>, <CODE>subactionPaths</CODE> <STRONG class=purple>must</STRONG> be a pointer to an array of <CODE>countSubactionPaths</CODE> valid <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrPath"><CODE>XrPath</CODE></A> values</P></LI>
<LI>
<P><A id=VUID-XrActionCreateInfo-localizedActionName-parameter href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-XrActionCreateInfo-localizedActionName-parameter"></A><CODE>localizedActionName</CODE> <STRONG class=purple>must</STRONG> be a null-terminated UTF-8 string whose length is less than or equal to <CODE>XR_MAX_LOCALIZED_ACTION_NAME_SIZE</CODE></P></LI></UL></DIV></DIV></DIV></DIV></DIV>
<DIV class=openblock>
<DIV class=content>
<DIV class=paragraph>
<P>The <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrActionType">XrActionType</A> parameter takes one of the following values:</P></DIV>
<DIV id=XrActionType class=paragraph>
<P></P></DIV>
<DIV class=listingblock>
<DIV class=content><PRE class="prettyprint highlight"><CODE data-lang="c++">typedef enum XrActionType {
    XR_ACTION_TYPE_BOOLEAN_INPUT = 1,
    XR_ACTION_TYPE_FLOAT_INPUT = 2,
    XR_ACTION_TYPE_VECTOR2F_INPUT = 3,
    XR_ACTION_TYPE_POSE_INPUT = 4,
    XR_ACTION_TYPE_VIBRATION_OUTPUT = 100,
    XR_ACTION_TYPE_MAX_ENUM = 0x7FFFFFFF
} XrActionType;</CODE></PRE></DIV></DIV>
<DIV class=sidebarblock>
<DIV class=content>
<DIV class=title>Enumerant Descriptions</DIV>
<DIV class=ulist>
<UL>
<LI>
<P><CODE>XR_ACTION_TYPE_BOOLEAN_INPUT</CODE>. The action can be passed to <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#xrGetActionStateBoolean">xrGetActionStateBoolean</A> to retrieve a boolean value.</P></LI>
<LI>
<P><CODE>XR_ACTION_TYPE_FLOAT_INPUT</CODE>. The action can be passed to <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#xrGetActionStateFloat">xrGetActionStateFloat</A> to retrieve a float value.</P></LI>
<LI>
<P><CODE>XR_ACTION_TYPE_VECTOR2F_INPUT</CODE>. The action can be passed to <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#xrGetActionStateVector2f">xrGetActionStateVector2f</A> to retrieve a 2D float vector.</P></LI>
<LI>
<P><CODE>XR_ACTION_TYPE_POSE_INPUT</CODE>. The action can can be passed to <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#xrCreateActionSpace">xrCreateActionSpace</A> to create a space.</P></LI>
<LI>
<P><CODE>XR_ACTION_TYPE_VIBRATION_OUTPUT</CODE>. The action can be passed to <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#xrApplyHapticFeedback">xrApplyHapticFeedback</A> to send a haptic event to the runtime.</P></LI></UL></DIV></DIV></DIV></DIV></DIV>
<DIV class=openblock>
<DIV class=content>
<DIV class=paragraph>
<P>The <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#xrDestroyAction">xrDestroyAction</A> function is defined as:</P></DIV>
<DIV id=xrDestroyAction class=paragraph>
<P></P></DIV>
<DIV class=listingblock>
<DIV class=content><PRE class="prettyprint highlight"><CODE data-lang="c++">XrResult xrDestroyAction(
    XrAction                                    action);</CODE></PRE></DIV></DIV>
<DIV class=sidebarblock>
<DIV class=content>
<DIV class=title>Parameter Descriptions</DIV>
<DIV class=ulist>
<UL>
<LI>
<P><CODE>action</CODE> is the action to destroy.</P></LI></UL></DIV></DIV></DIV>
<DIV class=paragraph>
<P>Action handles <STRONG class=purple>can</STRONG> be destroyed by calling <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#xrDestroyAction">xrDestroyAction</A>. Handles for actions that are part of an action set are automatically destroyed when the action set&#8217;s handle is destroyed.</P></DIV>
<DIV class=paragraph>
<P>The implementation <STRONG class=purple>must</STRONG> not destroy the underlying resources for an action when <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#xrDestroyAction">xrDestroyAction</A> is called. Those resources are still used to make <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#spaces-action-spaces-lifetime">action spaces locatable</A> and when processing action priority in <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#xrSyncActions">xrSyncActions</A>. Destroying the action handle removes the application&#8217;s access to these resources, but has no other change on actions.</P></DIV>
<DIV class=paragraph>
<P>Resources for all actions in an instance <STRONG class=purple>must</STRONG> be freed when the instance containing those actions sets is destroyed.</P></DIV>
<DIV class=sidebarblock>
<DIV class=content>
<DIV class=title>Valid Usage (Implicit)</DIV>
<DIV class=ulist>
<UL>
<LI>
<P><A id=VUID-xrDestroyAction-action-parameter href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-xrDestroyAction-action-parameter"></A><CODE>action</CODE> <STRONG class=purple>must</STRONG> be a valid <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrAction">XrAction</A> handle</P></LI></UL></DIV></DIV></DIV>
<DIV class=sidebarblock>
<DIV class=content>
<DIV class=title>Thread Safety</DIV>
<DIV class=ulist>
<UL>
<LI>
<P>Access to <CODE>action</CODE>, and any child handles, <STRONG class=purple>must</STRONG> be externally synchronized</P></LI></UL></DIV></DIV></DIV>
<DIV class=sidebarblock>
<DIV class=content>
<DIV class=title>Return Codes</DIV>
<DIV class=dlist>
<DL>
<DT class=hdlist1><A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#fundamentals-successcodes">Success</A></DT>
<DD>
<DIV class=ulist>
<UL>
<LI>
<P><CODE>XR_SUCCESS</CODE></P></LI></UL></DIV></DD>
<DT class=hdlist1><A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#fundamentals-errorcodes">Failure</A></DT>
<DD>
<DIV class=ulist>
<UL>
<LI>
<P><CODE>XR_ERROR_HANDLE_INVALID</CODE></P></LI></UL></DIV></DD></DL></DIV></DIV></DIV></DIV></DIV>
<DIV class=sect3>