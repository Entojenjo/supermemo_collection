<strong><font color="blue"> : </font></strong><H4 id=environment_blend_mode><A class=anchor href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#environment_blend_mode"></A>10.4.6. Environment Blend Mode</H4>
<DIV class=paragraph>
<P>After the compositor has blended and flattened all layers (including any layers added by the runtime itself), it will then present this image to the system&#8217;s display. The composited image will then blend with the user&#8217;s view of the physical world behind the displays in one of three modes, based on the application&#8217;s chosen <STRONG>environment blend mode</STRONG>. VR applications will generally choose the <CODE>XR_ENVIRONMENT_BLEND_MODE_OPAQUE</CODE> blend mode, while AR applications will generally choose either the <CODE>XR_ENVIRONMENT_BLEND_MODE_ADDITIVE</CODE> or <CODE>XR_ENVIRONMENT_BLEND_MODE_ALPHA_BLEND</CODE> mode.</P></DIV>
<DIV class=paragraph>
<P>Applications select their environment blend mode each frame as part of their call to <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#xrEndFrame">xrEndFrame</A>. The application can inspect the set of supported environment blend modes for a given system using <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#xrEnumerateEnvironmentBlendModes">xrEnumerateEnvironmentBlendModes</A>, and prepare their assets and rendering techniques differently based on the blend mode they choose. For example, a black shadow rendered using the <CODE>XR_ENVIRONMENT_BLEND_MODE_ADDITIVE</CODE> blend mode will appear transparent, and so an application in that mode <STRONG class=purple>may</STRONG> render a glow as a grounding effect around the black shadow to ensure the shadow can be seen. Similarly, an application designed for <CODE>XR_ENVIRONMENT_BLEND_MODE_OPAQUE</CODE> or <CODE>XR_ENVIRONMENT_BLEND_MODE_ADDITIVE</CODE> rendering <STRONG class=purple>may</STRONG> choose to leave garbage in their alpha channel as a side effect of a rendering optimization, but this garbage would appear as visible display artifacts if the environment blend mode was instead <CODE>XR_ENVIRONMENT_BLEND_MODE_ALPHA_BLEND</CODE>.</P></DIV>
<DIV class=paragraph>
<P>Not all systems will support all environment blend modes. For example, a VR headset may not support the <CODE>XR_ENVIRONMENT_BLEND_MODE_ADDITIVE</CODE> or <CODE>XR_ENVIRONMENT_BLEND_MODE_ALPHA_BLEND</CODE> modes unless it has video passthrough, while an AR headset with an additive display may not support the <CODE>XR_ENVIRONMENT_BLEND_MODE_OPAQUE</CODE> or <CODE>XR_ENVIRONMENT_BLEND_MODE_ALPHA_BLEND</CODE> modes.</P></DIV>
<DIV class=paragraph>
<P>For devices that can support multiple environment blend modes, such as AR phones with video passthrough, the runtime <STRONG class=purple>may</STRONG> optimize power consumption on the device in response to the environment blend mode that the application chooses each frame. For example, if an application on a video passthrough phone knows that it is currently rendering a 360-degree background covering all screen pixels, it can submit frames with an environment blend mode of <CODE>XR_ENVIRONMENT_BLEND_MODE_OPAQUE</CODE>, saving the runtime the cost of compositing a camera-based underlay of the physical world behind the application&#8217;s layers.</P></DIV>
<DIV class=openblock>
<DIV class=content>
<DIV class=paragraph>
<P>The <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#xrEnumerateEnvironmentBlendModes">xrEnumerateEnvironmentBlendModes</A> function is defined as:</P></DIV>
<DIV id=xrEnumerateEnvironmentBlendModes class=paragraph>
<P></P></DIV>
<DIV class=listingblock>
<DIV class=content><PRE class="prettyprint highlight"><CODE data-lang="c++">XrResult xrEnumerateEnvironmentBlendModes(
    XrInstance                                  instance,
    XrSystemId                                  systemId,
    XrViewConfigurationType                     viewConfigurationType,
    uint32_t                                    environmentBlendModeCapacityInput,
    uint32_t*                                   environmentBlendModeCountOutput,
    XrEnvironmentBlendMode*                     environmentBlendModes);</CODE></PRE></DIV></DIV>
<DIV class=sidebarblock>
<DIV class=content>
<DIV class=title>Parameter Descriptions</DIV>
<DIV class=ulist>
<UL>
<LI>
<P><CODE>instance</CODE> is the instance from which <CODE>systemId</CODE> was retrieved.</P></LI>
<LI>
<P><CODE>systemId</CODE> is the <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrSystemId"><CODE>XrSystemId</CODE></A> whose environment blend modes will be enumerated.</P></LI>
<LI>
<P><CODE>viewConfigurationType</CODE> is the <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrViewConfigurationType">XrViewConfigurationType</A> to enumerate.</P></LI>
<LI>
<P><CODE>environmentBlendModeCapacityInput</CODE> is the capacity of the <CODE>environmentBlendModes</CODE> array, or 0 to indicate a request to retrieve the required capacity.</P></LI>
<LI>
<P><CODE>environmentBlendModeCountOutput</CODE> is a pointer to the count of <CODE>environmentBlendModes</CODE> written, or a pointer to the required capacity in the case that <CODE>environmentBlendModeCapacityInput</CODE> is insufficient.</P></LI>
<LI>
<P><CODE>environmentBlendModes</CODE> is a pointer to an array of <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrEnvironmentBlendMode">XrEnvironmentBlendMode</A> values, but <STRONG class=purple>can</STRONG> be <CODE>NULL</CODE> if <CODE>environmentBlendModeCapacityInput</CODE> is 0.</P></LI>
<LI>
<P>See <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#buffer-size-parameters">Buffer Size Parameters</A> chapter for a detailed description of retrieving the required <CODE>environmentBlendModes</CODE> size.</P></LI></UL></DIV></DIV></DIV>
<DIV class=paragraph>
<P>Enumerates the set of environment blend modes that this runtime supports for a given view configuration of the system. Environment blend modes <STRONG class=purple>should</STRONG> be in order from highest to lowest runtime preference.</P></DIV>
<DIV class=paragraph>
<P>Runtimes <STRONG class=purple>must</STRONG> always return identical buffer contents from this enumeration for the given <CODE>systemId</CODE> and <CODE>viewConfigurationType</CODE> for the lifetime of the instance.</P></DIV>
<DIV class=sidebarblock>
<DIV class=content>
<DIV class=title>Valid Usage (Implicit)</DIV>
<DIV class=ulist>
<UL>
<LI>
<P><A id=VUID-xrEnumerateEnvironmentBlendModes-instance-parameter href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-xrEnumerateEnvironmentBlendModes-instance-parameter"></A><CODE>instance</CODE> <STRONG class=purple>must</STRONG> be a valid <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrInstance">XrInstance</A> handle</P></LI>
<LI>
<P><A id=VUID-xrEnumerateEnvironmentBlendModes-viewConfigurationType-parameter href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-xrEnumerateEnvironmentBlendModes-viewConfigurationType-parameter"></A><CODE>viewConfigurationType</CODE> <STRONG class=purple>must</STRONG> be a valid <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrViewConfigurationType">XrViewConfigurationType</A> value</P></LI>
<LI>
<P><A id=VUID-xrEnumerateEnvironmentBlendModes-environmentBlendModeCountOutput-parameter href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-xrEnumerateEnvironmentBlendModes-environmentBlendModeCountOutput-parameter"></A><CODE>environmentBlendModeCountOutput</CODE> <STRONG class=purple>must</STRONG> be a pointer to a <CODE>uint32_t</CODE> value</P></LI>
<LI>
<P><A id=VUID-xrEnumerateEnvironmentBlendModes-environmentBlendModes-parameter href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#VUID-xrEnumerateEnvironmentBlendModes-environmentBlendModes-parameter"></A>If <CODE>environmentBlendModeCapacityInput</CODE> is not <CODE>0</CODE>, <CODE>environmentBlendModes</CODE> <STRONG class=purple>must</STRONG> be a pointer to an array of <CODE>environmentBlendModeCapacityInput</CODE> <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrEnvironmentBlendMode">XrEnvironmentBlendMode</A> values</P></LI></UL></DIV></DIV></DIV>
<DIV class=sidebarblock>
<DIV class=content>
<DIV class=title>Return Codes</DIV>
<DIV class=dlist>
<DL>
<DT class=hdlist1><A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#fundamentals-successcodes">Success</A></DT>
<DD>
<DIV class=ulist>
<UL>
<LI>
<P><CODE>XR_SUCCESS</CODE></P></LI></UL></DIV></DD>
<DT class=hdlist1><A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#fundamentals-errorcodes">Failure</A></DT>
<DD>
<DIV class=ulist>
<UL>
<LI>
<P><CODE>XR_ERROR_VALIDATION_FAILURE</CODE></P></LI>
<LI>
<P><CODE>XR_ERROR_RUNTIME_FAILURE</CODE></P></LI>
<LI>
<P><CODE>XR_ERROR_HANDLE_INVALID</CODE></P></LI>
<LI>
<P><CODE>XR_ERROR_INSTANCE_LOST</CODE></P></LI>
<LI>
<P><CODE>XR_ERROR_SIZE_INSUFFICIENT</CODE></P></LI>
<LI>
<P><CODE>XR_ERROR_VIEW_CONFIGURATION_TYPE_UNSUPPORTED</CODE></P></LI>
<LI>
<P><CODE>XR_ERROR_SYSTEM_INVALID</CODE></P></LI></UL></DIV></DD></DL></DIV></DIV></DIV></DIV></DIV>
<DIV class=openblock>
<DIV class=content>
<DIV class=paragraph>
<P>The possible blend modes are specified by the <A href="https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrEnvironmentBlendMode">XrEnvironmentBlendMode</A> enumeration:</P></DIV>
<DIV id=XrEnvironmentBlendMode class=paragraph>
<P></P></DIV>
<DIV class=listingblock>
<DIV class=content><PRE class="prettyprint highlight"><CODE data-lang="c++">typedef enum XrEnvironmentBlendMode {
    XR_ENVIRONMENT_BLEND_MODE_OPAQUE = 1,
    XR_ENVIRONMENT_BLEND_MODE_ADDITIVE = 2,
    XR_ENVIRONMENT_BLEND_MODE_ALPHA_BLEND = 3,
    XR_ENVIRONMENT_BLEND_MODE_MAX_ENUM = 0x7FFFFFFF
} XrEnvironmentBlendMode;</CODE></PRE></DIV></DIV>
<DIV class=sidebarblock>
<DIV class=content>
<DIV class=title>Enumerant Descriptions</DIV>
<DIV class=ulist>
<UL>
<LI>
<P><CODE>XR_ENVIRONMENT_BLEND_MODE_OPAQUE</CODE>. The composition layers will be displayed with no view of the physical world behind them. The composited image will be interpreted as an RGB image, ignoring the composited alpha channel. This is the typical mode for VR experiences, although this mode can also be supported on devices that support video passthrough.</P></LI>
<LI>
<P><CODE>XR_ENVIRONMENT_BLEND_MODE_ADDITIVE</CODE>. The composition layers will be additively blended with the real world behind the display. The composited image will be interpreted as an RGB image, ignoring the composited alpha channel during the additive blending. This will cause black composited pixels to appear transparent. This is the typical mode for an AR experience on a see-through headset with an additive display, although this mode can also be supported on devices that support video passthrough.</P></LI>
<LI>
<P><CODE>XR_ENVIRONMENT_BLEND_MODE_ALPHA_BLEND</CODE>. The composition layers will be alpha-blended with the real world behind the display. The composited image will be interpreted as an RGBA image, with the composited alpha channel determining each pixel&#8217;s level of blending with the real world behind the display. This is the typical mode for an AR experience on a phone or headset that supports video passthrough.</P></LI></UL></DIV></DIV></DIV></DIV></DIV></DIV></DIV></DIV></DIV>
<DIV class=sect1>