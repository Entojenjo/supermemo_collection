<H1><SPAN id=A_first_example class=mw-headline>A first example</SPAN></H1>
<P>Let's start with an example which typesets two centred paragraphs by writing them inside a <CODE>center</CODE> environment. Note how a new paragraph is started by inserting a blank line between them&#8212;although that's a commonly-used method, it's not the only way to start a new paragraph. </P>
<DIV class=example>
<DIV class=code>
<DIV class="mw-highlight mw-content-ltr" dir=ltr><PRE><SPAN></SPAN><SPAN class=k>\begin</SPAN><SPAN class=nb>{</SPAN>center<SPAN class=nb>}</SPAN>
Example 1: The following paragraph (given in quotes) is an 
example of centred alignment using the center environment. 

``La<SPAN class=k>\TeX</SPAN><SPAN class=nb>{}</SPAN> is a document preparation system and document markup 
language. <SPAN class=k>\LaTeX</SPAN><SPAN class=nb>{}</SPAN> uses the <SPAN class=k>\TeX</SPAN><SPAN class=nb>{}</SPAN> typesetting program for formatting 
its output, and is itself written in the <SPAN class=k>\TeX</SPAN><SPAN class=nb>{}</SPAN> macro language. 
<SPAN class=k>\LaTeX</SPAN><SPAN class=nb>{}</SPAN> is not the name of a particular (executable) typesetting program, but 
refers to the suite of commands (<SPAN class=k>\TeX</SPAN><SPAN class=nb>{}</SPAN> macros) which form the markup 
conventions used to typeset <SPAN class=k>\LaTeX</SPAN><SPAN class=nb>{}</SPAN> documents."
<SPAN class=k>\end</SPAN><SPAN class=nb>{</SPAN>center<SPAN class=nb>}</SPAN>
</PRE></DIV></DIV></DIV>
<P><A class=open-in-overleaf href="https://www.overleaf.com/docs?engine=pdflatex&amp;snip_name=Basic+paragraph+formatting&amp;snip=%5Cdocumentclass%7Barticle%7D%0A%25+Using+the+geometry+package+with+a+small%0A%25+page+size+to+create+the+article+graphic%0A%5Cusepackage%5Bpaperheight%3D6in%2C%0A+++paperwidth%3D5in%2C%0A+++top%3D10mm%2C%0A+++bottom%3D20mm%2C%0A+++left%3D10mm%2C%0A+++right%3D10mm%5D%7Bgeometry%7D%0A%5Cbegin%7Bdocument%7D%0A%5Cbegin%7Bcenter%7D%0AExample+1%3A+The+following+paragraph+%28given+in+quotes%29+is+an+%0Aexample+of+centred+alignment+using+the+center+environment.+%0A%0A%60%60La%5CTeX%7B%7D+is+a+document+preparation+system+and+document+markup+%0Alanguage.+%5CLaTeX%7B%7D+uses+the+%5CTeX%7B%7D+typesetting+program+for+formatting+%0Aits+output%2C+and+is+itself+written+in+the+%5CTeX%7B%7D+macro+language.+%0A%5CLaTeX%7B%7D+is+not+the+name+of+a+particular+%28executable%29+typesetting+program%2C+but+%0Arefers+to+the+suite+of+commands+%28%5CTeX%7B%7D+macros%29+which+form+the+markup+%0Aconventions+used+to+typeset+%5CLaTeX%7B%7D+documents.%22%0A%5Cend%7Bcenter%7D%0A%5Cend%7Bdocument%7D" target=_blank><I class="fa fa-share"></I>&nbsp;Open this example in Overleaf</A> </P>
<P>This example produces the following output: </P>
<P><img alt="LaTeX centred paragraph example" src="https://sharelatex-wiki-cdn-671420.c.cdn77.org/learn-scripts/images/1/19/OLV2newparaex1b.png" width=600 height=188 decoding="async"> </P>
<H1><SPAN id=Starting_a_new_paragraph class=mw-headline>Starting a new paragraph</SPAN></H1>
<P>As noted above, one way to start a new paragraph is by inserting a blank line but the following code snippet shows an alternative solution which uses the <CODE>\par</CODE> command: </P>
<DIV class=example>
<DIV class=code>
<DIV class="mw-highlight mw-content-ltr" dir=ltr><PRE><SPAN></SPAN>This is text contained in the first paragraph. 
This is text contained in the first paragraph. 
This is text contained in the first paragraph.<SPAN class=k>\par</SPAN>
This is text contained in the second paragraph. 
This is text contained in the second paragraph.
This is text contained in the second paragraph.
</PRE></DIV></DIV></DIV>
<P><A class=open-in-overleaf href="https://www.overleaf.com/docs?engine=pdflatex&amp;snip_name=Starting+a+new+paragraph&amp;snip=%5Cdocumentclass%7Barticle%7D%0A%25+Using+the+geometry+package+with+a+small%0A%25+page+size+to+create+the+article+graphic%0A%5Cusepackage%5Bpaperheight%3D6in%2C%0A+++paperwidth%3D5in%2C%0A+++top%3D10mm%2C%0A+++bottom%3D20mm%2C%0A+++left%3D10mm%2C%0A+++right%3D10mm%5D%7Bgeometry%7D%0A%5Cbegin%7Bdocument%7D%0AThis+is+text+contained+in+the+first+paragraph.+%0AThis+is+text+contained+in+the+first+paragraph.+%0AThis+is+text+contained+in+the+first+paragraph.%5Cpar%0AThis+is+text+contained+in+the+second+paragraph.+%0AThis+is+text+contained+in+the+second+paragraph.%0AThis+is+text+contained+in+the+second+paragraph.%0A%5Cend%7Bdocument%7D" target=_blank><I class="fa fa-share"></I>&nbsp;Open this example in Overleaf</A> </P>
<P>This example produces the following output: </P>
<P><img alt="Using the \par command to create a paragraph" src="https://sharelatex-wiki-cdn-671420.c.cdn77.org/learn-scripts/images/1/17/OLV2paraex3.png" width=600 height=120 decoding="async"> </P>
<H1><SPAN id=Paragraph_alignment class=mw-headline>Paragraph alignment</SPAN></H1>
<P>By default paragraphs in LaTeX are fully justified, i.e. flush with both the left and right margins. If you would like to typeset an <EM>unjustified</EM> paragraph you can use the <CODE>flushleft</CODE> or <CODE>flushright</CODE> environments. </P>
<H2><SPAN id=flushleft_and_flushright_environments class=mw-headline><CODE>flushleft</CODE> and <CODE>flushright</CODE> environments</SPAN></H2>
<P>The next example demonstrates typesetting a paragraph within the <CODE>flushleft</CODE> and <CODE>flushright</CODE> environments&#8212;for an example of the <CODE>center</CODE> environment see the section <A href="https://www.overleaf.com/learn/latex/Paragraphs_and_new_lines#A_first_example">A first example</A>. </P>
<DIV class=example>
<DIV class=code>
<DIV class="mw-highlight mw-content-ltr" dir=ltr><PRE><SPAN></SPAN><SPAN class=k>\section*</SPAN><SPAN class=nb>{</SPAN>A paragraph typeset flush left<SPAN class=nb>}</SPAN>

<SPAN class=k>\begin</SPAN><SPAN class=nb>{</SPAN>flushleft<SPAN class=nb>}</SPAN>
La<SPAN class=k>\TeX</SPAN><SPAN class=nb>{}</SPAN> is a document preparation system and document markup 
language. <SPAN class=k>\LaTeX</SPAN><SPAN class=nb>{}</SPAN> uses the <SPAN class=k>\TeX</SPAN><SPAN class=nb>{}</SPAN> typesetting program for formatting 
its output, and is itself written in the <SPAN class=k>\TeX</SPAN><SPAN class=nb>{}</SPAN> macro language. 
<SPAN class=k>\LaTeX</SPAN><SPAN class=nb>{}</SPAN> is not the name of a particular (executable) typesetting program, but 
refers to the suite of commands (<SPAN class=k>\TeX</SPAN><SPAN class=nb>{}</SPAN> macros) which form the markup 
conventions used to typeset <SPAN class=k>\LaTeX</SPAN><SPAN class=nb>{}</SPAN> documents.
<SPAN class=k>\end</SPAN><SPAN class=nb>{</SPAN>flushleft<SPAN class=nb>}</SPAN>

<SPAN class=k>\section*</SPAN><SPAN class=nb>{</SPAN>A paragraph typeset flush right<SPAN class=nb>}</SPAN>

<SPAN class=k>\begin</SPAN><SPAN class=nb>{</SPAN>flushright<SPAN class=nb>}</SPAN>
La<SPAN class=k>\TeX</SPAN><SPAN class=nb>{}</SPAN> is a document preparation system and document markup 
language. <SPAN class=k>\LaTeX</SPAN><SPAN class=nb>{}</SPAN> uses the <SPAN class=k>\TeX</SPAN><SPAN class=nb>{}</SPAN> typesetting program for formatting 
its output, and is itself written in the <SPAN class=k>\TeX</SPAN><SPAN class=nb>{}</SPAN> macro language. 
<SPAN class=k>\LaTeX</SPAN><SPAN class=nb>{}</SPAN> is not the name of a particular (executable) typesetting program, but 
refers to the suite of commands (<SPAN class=k>\TeX</SPAN><SPAN class=nb>{}</SPAN> macros) which form the markup 
conventions used to typeset <SPAN class=k>\LaTeX</SPAN><SPAN class=nb>{}</SPAN> documents.
<SPAN class=k>\end</SPAN><SPAN class=nb>{</SPAN>flushright<SPAN class=nb>}</SPAN>
</PRE></DIV></DIV></DIV>
<P><A class=open-in-overleaf href="https://www.overleaf.com/docs?engine=pdflatex&amp;snip_name=Example+of+flushleft+and+flushright&amp;snip=%5Cdocumentclass%7Barticle%7D%0A%25+Using+the+geometry+package+with+a+small%0A%25+page+size+to+create+the+article+graphic%0A%5Cusepackage%5Bpaperheight%3D6in%2C%0A+++paperwidth%3D5in%2C%0A+++top%3D10mm%2C%0A+++bottom%3D20mm%2C%0A+++left%3D10mm%2C%0A+++right%3D10mm%5D%7Bgeometry%7D%0A%5Cbegin%7Bdocument%7D%0A%5Csection%2A%7BA+paragraph+typeset+flush+left%7D%0A%0A%5Cbegin%7Bflushleft%7D%0ALa%5CTeX%7B%7D+is+a+document+preparation+system+and+document+markup+%0Alanguage.+%5CLaTeX%7B%7D+uses+the+%5CTeX%7B%7D+typesetting+program+for+formatting+%0Aits+output%2C+and+is+itself+written+in+the+%5CTeX%7B%7D+macro+language.+%0A%5CLaTeX%7B%7D+is+not+the+name+of+a+particular+%28executable%29+typesetting+program%2C+but+%0Arefers+to+the+suite+of+commands+%28%5CTeX%7B%7D+macros%29+which+form+the+markup+%0Aconventions+used+to+typeset+%5CLaTeX%7B%7D+documents.%0A%5Cend%7Bflushleft%7D%0A%0A%5Csection%2A%7BA+paragraph+typeset+flush+right%7D%0A%0A%5Cbegin%7Bflushright%7D%0ALa%5CTeX%7B%7D+is+a+document+preparation+system+and+document+markup+%0Alanguage.+%5CLaTeX%7B%7D+uses+the+%5CTeX%7B%7D+typesetting+program+for+formatting+%0Aits+output%2C+and+is+itself+written+in+the+%5CTeX%7B%7D+macro+language.+%0A%5CLaTeX%7B%7D+is+not+the+name+of+a+particular+%28executable%29+typesetting+program%2C+but+%0Arefers+to+the+suite+of+commands+%28%5CTeX%7B%7D+macros%29+which+form+the+markup+%0Aconventions+used+to+typeset+%5CLaTeX%7B%7D+documents.%0A%5Cend%7Bflushright%7D%0A%5Cend%7Bdocument%7D" target=_blank><I class="fa fa-share"></I>&nbsp;Open this example in Overleaf</A> </P>
<P>This example produces the following output: </P>
<P><img alt="LaTeX flushleft anf flushright environments" src="https://sharelatex-wiki-cdn-671420.c.cdn77.org/learn-scripts/images/6/67/OLV2paraex4.png" width=600 height=411 decoding="async"> </P>
<H2><SPAN id=\raggedright_and_\raggedleft></SPAN><SPAN id=.5Craggedright_and_.5Craggedleft class=mw-headline><CODE>\raggedright</CODE> and <CODE>\raggedleft</CODE></SPAN></H2>
<P>An alternative to using environments such as <CODE>flushleft</CODE>, <CODE>flushright</CODE> or <CODE>center</CODE> are the so-called "switch" commands: </P>
<UL>
<LI><CODE>\raggedright</CODE>, an alternative to using the <CODE>flushleft</CODE> environment 
<LI><CODE>\raggedleft</CODE>, an alternative to using the <CODE>flushright</CODE> environment 
<LI><CODE>\centering</CODE>, an alternative to using the <CODE>center</CODE> environment</LI></UL>
<P>These switch commands change text alignment from the point they are inserted down to the end of the document&#8212;unless their effect(s) are restricted to a group or changed by another switch command. </P>
<P>In the following example, the effects of <CODE>\raggedright</CODE>, <CODE>\raggedleft</CODE> and <CODE>\centering</CODE> are localized because they are used within the group created by <CODE>\begingroup ... \endgroup</CODE>. In addition, note that in each case the paragraph text is followed by a blank line, before the <CODE>\endgroup</CODE> command, which triggers LaTeX to typeset the paragraph whilst the settings applied by <CODE>\raggedright</CODE>, <CODE>\raggedleft</CODE> and <CODE>\centering</CODE> are still active. </P>
<DIV class=example>
<DIV class=code>
<DIV class="mw-highlight mw-content-ltr" dir=ltr><PRE><SPAN></SPAN><SPAN class=k>\section*</SPAN><SPAN class=nb>{</SPAN>A fully justified  typeset paragraph<SPAN class=nb>}</SPAN> 
<SPAN class=k>\LaTeX</SPAN><SPAN class=nb>{}</SPAN> is not the name of a particular (executable) typesetting program, but refers to the suite of commands (<SPAN class=k>\TeX</SPAN><SPAN class=nb>{}</SPAN> macros) which form the markup 
conventions used to typeset <SPAN class=k>\LaTeX</SPAN><SPAN class=nb>{}</SPAN> documents.

<SPAN class=k>\section*</SPAN><SPAN class=nb>{</SPAN>A paragraph typeset using <SPAN class=k>\texttt</SPAN><SPAN class=nb>{</SPAN><SPAN class=k>\string\raggedright</SPAN><SPAN class=nb>}}</SPAN>

<SPAN class=k>\begingroup</SPAN>
<SPAN class=k>\raggedright</SPAN> 
<SPAN class=k>\LaTeX</SPAN><SPAN class=nb>{}</SPAN> is not the name of a particular (executable) typesetting program, but refers to the suite of commands (<SPAN class=k>\TeX</SPAN><SPAN class=nb>{}</SPAN> macros) which form the markup 
conventions used to typeset <SPAN class=k>\LaTeX</SPAN><SPAN class=nb>{}</SPAN> documents.

<SPAN class=k>\endgroup</SPAN>

<SPAN class=k>\section*</SPAN><SPAN class=nb>{</SPAN>A paragraph typeset using <SPAN class=k>\texttt</SPAN><SPAN class=nb>{</SPAN><SPAN class=k>\string\raggeleft</SPAN><SPAN class=nb>}}</SPAN>

<SPAN class=k>\begingroup</SPAN>
<SPAN class=k>\raggedleft</SPAN> 
<SPAN class=k>\LaTeX</SPAN><SPAN class=nb>{}</SPAN> is not the name of a particular (executable) typesetting program, but refers to the suite of commands (<SPAN class=k>\TeX</SPAN><SPAN class=nb>{}</SPAN> macros) which form the markup 
conventions used to typeset <SPAN class=k>\LaTeX</SPAN><SPAN class=nb>{}</SPAN> documents.

<SPAN class=k>\endgroup</SPAN>

<SPAN class=k>\section*</SPAN><SPAN class=nb>{</SPAN>A paragraph typeset using <SPAN class=k>\texttt</SPAN><SPAN class=nb>{</SPAN><SPAN class=k>\string\centering</SPAN><SPAN class=nb>}}</SPAN>

<SPAN class=k>\begingroup</SPAN>
<SPAN class=k>\centering</SPAN> 
<SPAN class=k>\LaTeX</SPAN><SPAN class=nb>{}</SPAN> is not the name of a particular (executable) typesetting program, but refers to the suite of commands (<SPAN class=k>\TeX</SPAN><SPAN class=nb>{}</SPAN> macros) which form the markup 
conventions used to typeset <SPAN class=k>\LaTeX</SPAN><SPAN class=nb>{}</SPAN> documents.

<SPAN class=k>\endgroup</SPAN>
</PRE></DIV></DIV></DIV>
<P><A class=open-in-overleaf href="https://www.overleaf.com/docs?engine=pdflatex&amp;snip_name=Non-justified+paragraph+formatting+options&amp;snip=%5Cdocumentclass%7Barticle%7D%0A%25+Using+the+geometry+package+with+a+small%0A%25+page+size+to+create+the+article+graphic%0A%5Cusepackage%5Bpaperheight%3D6in%2C%0A+++paperwidth%3D5in%2C%0A+++top%3D10mm%2C%0A+++bottom%3D20mm%2C%0A+++left%3D10mm%2C%0A+++right%3D10mm%5D%7Bgeometry%7D%0A%5Cbegin%7Bdocument%7D%0A%5Csection%2A%7BA+fully+justified++typeset+paragraph%7D+%0A%5CLaTeX%7B%7D+is+not+the+name+of+a+particular+%28executable%29+typesetting+program%2C+but+refers+to+the+suite+of+commands+%28%5CTeX%7B%7D+macros%29+which+form+the+markup+%0Aconventions+used+to+typeset+%5CLaTeX%7B%7D+documents.%0A%0A%5Csection%2A%7BA+paragraph+typeset+using+%5Ctexttt%7B%5Cstring%5Craggedright%7D%7D%0A%0A%5Cbegingroup%0A%5Craggedright+%0A%5CLaTeX%7B%7D+is+not+the+name+of+a+particular+%28executable%29+typesetting+program%2C+but+refers+to+the+suite+of+commands+%28%5CTeX%7B%7D+macros%29+which+form+the+markup+%0Aconventions+used+to+typeset+%5CLaTeX%7B%7D+documents.%0A%0A%5Cendgroup%0A%0A%5Csection%2A%7BA+paragraph+typeset+using+%5Ctexttt%7B%5Cstring%5Craggeleft%7D%7D%0A%0A%5Cbegingroup%0A%5Craggedleft+%0A%5CLaTeX%7B%7D+is+not+the+name+of+a+particular+%28executable%29+typesetting+program%2C+but+refers+to+the+suite+of+commands+%28%5CTeX%7B%7D+macros%29+which+form+the+markup+%0Aconventions+used+to+typeset+%5CLaTeX%7B%7D+documents.%0A%0A%5Cendgroup%0A%0A%5Csection%2A%7BA+paragraph+typeset+using+%5Ctexttt%7B%5Cstring%5Ccentering%7D%7D%0A%0A%5Cbegingroup%0A%5Ccentering+%0A%5CLaTeX%7B%7D+is+not+the+name+of+a+particular+%28executable%29+typesetting+program%2C+but+refers+to+the+suite+of+commands+%28%5CTeX%7B%7D+macros%29+which+form+the+markup+%0Aconventions+used+to+typeset+%5CLaTeX%7B%7D+documents.%0A%0A%5Cendgroup%0A%5Cend%7Bdocument%7D" target=_blank><I class="fa fa-share"></I>&nbsp;Open this example in Overleaf</A> </P>
<P>This example produces the following output: </P>
<P><img alt="Various alignment options for typesetting a LaTeX paragraph" src="https://sharelatex-wiki-cdn-671420.c.cdn77.org/learn-scripts/images/0/09/OLV2paraex6.png" width=600 height=573 decoding="async"> </P>
<P>For more detailed information and examples of text alignment see the <A title="Text alignment" href="https://www.overleaf.com/learn/latex/Text_alignment">Overleaf article on text alignment </A>which also discusses using the <A class="external text" href="https://ctan.org/pkg/ragged2e?lang=en" rel=nofollow>ragged2e LaTeX package</A> to typeset ragged text and configure hyphenation. </P>
<H1><SPAN id=Paragraph_indentation class=mw-headline>Paragraph indentation</SPAN></H1>
<P>By default new paragraphs are usually indented by an amount controlled by a parameter called <CODE>\parindent</CODE> whose value can be set using the command <CODE>\setlength</CODE>; for example: </P>
<DIV class=example>
<DIV class=code>
<DIV class="mw-highlight mw-content-ltr" dir=ltr><PRE><SPAN></SPAN><SPAN class=k>\setlength</SPAN><SPAN class=nb>{</SPAN><SPAN class=k>\parindent</SPAN><SPAN class=nb>}{</SPAN>20pt<SPAN class=nb>}</SPAN>
</PRE></DIV></DIV></DIV>
<P>sets <CODE>\parindent</CODE> to 20pt. You can avoid indentation by setting <CODE>\parindent</CODE> to 0pt (or 0mm, 0cm etc) or using the command <CODE>\noindent</CODE> at the beginning of the paragraph. By default LaTeX does not indent the first paragraph contained in a document section as demonstrated in the following example: </P>
<DIV class=example>
<DIV class=code>
<DIV class="mw-highlight mw-content-ltr" dir=ltr><PRE><SPAN></SPAN><SPAN class=k>\setlength</SPAN><SPAN class=nb>{</SPAN><SPAN class=k>\parindent</SPAN><SPAN class=nb>}{</SPAN>20pt<SPAN class=nb>}</SPAN>

<SPAN class=k>\section*</SPAN><SPAN class=nb>{</SPAN>This is a section<SPAN class=nb>}</SPAN>
<SPAN class=k>\textbf</SPAN><SPAN class=nb>{</SPAN>First paragraph<SPAN class=nb>}</SPAN> of a section which, as you can see, is not indented. This is more text in the paragraph. This is more text in the paragraph.

<SPAN class=k>\textbf</SPAN><SPAN class=nb>{</SPAN>Second paragraph<SPAN class=nb>}</SPAN>. As you can see it is indented. This is more text in the paragraph. This is more text in the paragraph. 

<SPAN class=k>\noindent\textbf</SPAN><SPAN class=nb>{</SPAN>Third paragraph<SPAN class=nb>}</SPAN>. This too is not indented due to use of <SPAN class=k>\texttt</SPAN><SPAN class=nb>{</SPAN><SPAN class=k>\string\noindent</SPAN><SPAN class=nb>}</SPAN>. This is more text in the paragraph. This is more text in the paragraph.  The current value of <SPAN class=k>\verb</SPAN>|<SPAN class=k>\parindent</SPAN>| is <SPAN class=k>\the\parindent</SPAN>. This is more text in the paragraph.
</PRE></DIV></DIV></DIV>
<P><A class=open-in-overleaf href="https://www.overleaf.com/docs?engine=pdflatex&amp;snip_name=Basic+paragraph+indentation&amp;snip=%5Cdocumentclass%7Barticle%7D%0A%25+Using+the+geometry+package+with+a+small%0A%25+page+size+to+create+the+article+graphic%0A%5Cusepackage%5Bpaperheight%3D6in%2C%0A+++paperwidth%3D5in%2C%0A+++top%3D10mm%2C%0A+++bottom%3D20mm%2C%0A+++left%3D10mm%2C%0A+++right%3D10mm%5D%7Bgeometry%7D%0A%5Cbegin%7Bdocument%7D%0A%5Csetlength%7B%5Cparindent%7D%7B20pt%7D%0A%0A%5Csection%2A%7BThis+is+a+section%7D%0A%5Ctextbf%7BFirst+paragraph%7D+of+a+section+which%2C+as+you+can+see%2C+is+not+indented.+This+is+more+text+in+the+paragraph.+This+is+more+text+in+the+paragraph.%0A%0A%5Ctextbf%7BSecond+paragraph%7D.+As+you+can+see+it+is+indented.+This+is+more+text+in+the+paragraph.+This+is+more+text+in+the+paragraph.+%0A%0A%5Cnoindent%5Ctextbf%7BThird+paragraph%7D.+This+too+is+not+indented+due+to+use+of+%5Ctexttt%7B%5Cstring%5Cnoindent%7D.+This+is+more+text+in+the+paragraph.+This+is+more+text+in+the+paragraph.++The+current+value+of+%5Cverb%7C%5Cparindent%7C+is+%5Cthe%5Cparindent.+This+is+more+text+in+the+paragraph.%0A%5Cend%7Bdocument%7D" target=_blank><I class="fa fa-share"></I>&nbsp;Open this example in Overleaf</A> </P>
<P>This example produces the following output: </P>
<P><img alt="Demonstrating LaTeX paragraph indentation" src="https://sharelatex-wiki-cdn-671420.c.cdn77.org/learn-scripts/images/d/d2/OLV2paraex7.png" width=600 height=239 decoding="async"> </P>
<H2><SPAN id=Notes_on_indentation-related_commands class=mw-headline>Notes on indentation-related commands</SPAN></H2>
<P>Paragraph indentation is controlled or influenced by three commands: </P>
<UL>
<LI><CODE>\parindent</CODE>: a parameter which stores the current size of the paragraph indent 
<LI><CODE>\indent</CODE>: the effect of this command depends where it is used: 
<UL>
<LI>in a horizontal mode (inside a paragraph or an <CODE>\hbox</CODE>) or math mode it inserts a space (an empty box) of width <CODE>\parindent</CODE> 
<LI>in a vertical mode (between paragraphs or in a <CODE>\vbox</CODE>) it triggers the start a new indented paragraph</LI></UL>
<LI><CODE>\noindent</CODE>: the effect of this command also depends where it is used: 
<UL>
<LI>in a vertical mode (between paragraphs or in a <CODE>\vbox</CODE>) it also triggers a new <EM>non-indented</EM> paragraph 
<LI>in a horizontal mode (inside a paragraph or an <CODE>\hbox</CODE>) or math mode it has no effect: it is ignored</LI></UL></LI></UL>
<P>The following example demonstrates <CODE>\indent</CODE>: </P>
<DIV class=example>
<DIV class=code>
<DIV class="mw-highlight mw-content-ltr" dir=ltr><PRE><SPAN></SPAN><SPAN class=k>\documentclass</SPAN><SPAN class=nb>{</SPAN>article<SPAN class=nb>}</SPAN>
<SPAN class=c>% Using the geometry package with a small</SPAN>
<SPAN class=c>% page size to create the article graphic</SPAN>
<SPAN class=k>\usepackage</SPAN>[paperheight=6in,
   paperwidth=5in,
   top=10mm,
   bottom=20mm,
   left=10mm,
   right=10mm]<SPAN class=nb>{</SPAN>geometry<SPAN class=nb>}</SPAN>
<SPAN class=k>\begin</SPAN><SPAN class=nb>{</SPAN>document<SPAN class=nb>}</SPAN>
<SPAN class=k>\noindent</SPAN> A new paragraph with some text, then an <SPAN class=k>\verb</SPAN>|<SPAN class=k>\indent</SPAN>|<SPAN class=k>\indent</SPAN> command. Next, some inline math which also has an indent <SPAN class=s>$</SPAN><SPAN class=nb>y</SPAN><SPAN class=nv>\indent</SPAN><SPAN class=nb> x</SPAN><SPAN class=s>$</SPAN>. <SPAN class=k>\verb</SPAN>|<SPAN class=k>\indent</SPAN>| also works when used in an <SPAN class=k>\verb</SPAN>|<SPAN class=k>\hbox</SPAN>| such as <SPAN class=k>\verb</SPAN>|<SPAN class=k>\hbox</SPAN><SPAN class=nb>{</SPAN>A<SPAN class=k>\indent</SPAN> B<SPAN class=nb>}</SPAN>| which produces <SPAN class=k>\hbox</SPAN><SPAN class=nb>{</SPAN>A<SPAN class=k>\indent</SPAN> B<SPAN class=nb>}</SPAN>.
<SPAN class=k>\end</SPAN><SPAN class=nb>{</SPAN>document<SPAN class=nb>}</SPAN>
</PRE></DIV></DIV></DIV>
<P><A class=open-in-overleaf href="https://www.overleaf.com/docs?engine=pdflatex&amp;snip_name=Example+of+the+%5Cindent+command&amp;snip=%5Cdocumentclass%7Barticle%7D%0A%25+Using+the+geometry+package+with+a+small%0A%25+page+size+to+create+the+article+graphic%0A%5Cusepackage%5Bpaperheight%3D6in%2C%0A+++paperwidth%3D5in%2C%0A+++top%3D10mm%2C%0A+++bottom%3D20mm%2C%0A+++left%3D10mm%2C%0A+++right%3D10mm%5D%7Bgeometry%7D%0A%5Cbegin%7Bdocument%7D%0A%5Cnoindent+A+new+paragraph+with+some+text%2C+then+an+%5Cverb%7C%5Cindent%7C%5Cindent+command.+Next%2C+some+inline+math+which+also+has+an+indent+%24y%5Cindent+x%24.+%5Cverb%7C%5Cindent%7C+also+works+when+used+in+an+%5Cverb%7C%5Chbox%7C+such+as+%5Cverb%7C%5Chbox%7BA%5Cindent+B%7D%7C+which+produces+%5Chbox%7BA%5Cindent+B%7D.%0A%5Cend%7Bdocument%7D" target=_blank><I class="fa fa-share"></I>&nbsp;Open this example in Overleaf</A> </P>
<P>This example produces the following output: </P>
<P><img alt="Demonstrating the \indent command" src="https://sharelatex-wiki-cdn-671420.c.cdn77.org/learn-scripts/images/d/d9/OLV2paraex8.png" width=600 height=94 decoding="async"> </P>
<H1><SPAN id=Further_Reading class=mw-headline>Further Reading</SPAN></H1>
<P>For more information check </P>
<UL>
<LI><A title="Paragraph formatting" class=mw-redirect href="https://www.overleaf.com/learn/latex/Paragraph_formatting">Paragraph formatting</A> 
<LI><A title="Text alignment" href="https://www.overleaf.com/learn/latex/Text_alignment">Text alignment</A> 
<LI><A title="Lengths in LaTeX" href="https://www.overleaf.com/learn/latex/Lengths_in_LaTeX">Lengths in LaTeX</A> 
<LI><A title="Line breaks and blank spaces" href="https://www.overleaf.com/learn/latex/Line_breaks_and_blank_spaces">Line breaks and blank spaces</A> 
<LI><A title=Lists href="https://www.overleaf.com/learn/latex/Lists">Lists</A> 
<LI><A title="Bold, italics and underlining" href="https://www.overleaf.com/learn/latex/Bold%2C_italics_and_underlining">Bold, italics and underlining</A> 
<LI><A class="external text" href="http://www.ctan.org/tex-archive/info/lshort/" rel=nofollow>The not so short introduction to <SPAN class=texhtml style="FONT-FAMILY: 'CMU Serif', cmr10, LMRoman10-Regular, 'Times New Roman', 'Nimbus Roman No9 L', Times, serif">L<SPAN style="FONT-SIZE: 70%; VERTICAL-ALIGN: 0.3em; TEXT-TRANSFORM: uppercase; MARGIN-LEFT: -0.36em; LINE-HEIGHT: 0; MARGIN-RIGHT: -0.15em">a</SPAN>T<SPAN style="VERTICAL-ALIGN: -0.5ex; TEXT-TRANSFORM: uppercase; MARGIN-LEFT: -0.16em; LINE-HEIGHT: 0; MARGIN-RIGHT: -0.12em">e</SPAN>X<SPAN style="MARGIN-LEFT: 0.15em">2</SPAN><SPAN style="VERTICAL-ALIGN: -0.35ex; FONT-STYLE: italic; LINE-HEIGHT: 0">&#949;</SPAN></SPAN></A></LI></UL>
<DIV></DIV>
<DIV></DIV>
<DIV></DIV>
<DIV></DIV>