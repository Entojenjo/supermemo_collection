SuperMemo Report
   Activity: Checking the integrity of the collection
   Collection: c:\users\memory\supermemo\systems\incremental
   Date: Saturday, November 5, 2022, 12:11:51 PM
   SuperMemo 18 (Build 18.05, Oct 29, 2020)

Checking 1,841 element records (ElementInfo.dat)
Turning on tasklist verification along element data integrity checks
Verifying integrity of element data (1841 elements in ElementInfo.dat)
Verifying knowledge tree (1,841 elements in Contents.dat)
136 children at Topic #1,320: The OpenXR Specification Copyright (c) 2017-2022
Verifying the priority queue of 675 elements
Verifying repetition schedule (Workload.dat and ElementInfo.dat)
Verifying pending queue (Intact.dat)
Verifying A-Factor, Lapses and Repetitions distributions
Recovering registries and filespace references
Rebuilding Text registry (1488 members in a 4742 member file)
Rebuilding Template registry (24 members in a 26 member file)
Rebuilding Font registry (0 members in a 0 member file)
Rebuilding Reference registry (157 members in a 253 member file)
Rebuilding Style registry (0 members in a 0 member file)
Rebuilding Script registry (0 members in a 0 member file)
Rebuilding Program registry (95 members in a 223 member file)
Rebuilding Ole registry (0 members in a 0 member file)
Rebuilding Video registry (1 members in a 1 member file)
Rebuilding Image registry (30 members in a 147 member file)
Rebuilding Sound registry (0 members in a 0 member file)
Rebuilding Tasklist registry (1 members in a 3 member file)
Rebuilding Concept registry (43 members in a 61 member file)
Rebuilding Link registry (3 members in a 9 member file)
Registering components
Transferring templates
Registering references
Registering image references
Registration of links for 3 members in a 9 member file
Registration of concepts for 43 members in a 61 member file
Verifying Text registry
Warning! Registry member not used
   Text #3: HTML: <P>&nbsp;</P>
Warning! Registry member not used
   Text #100: HTML: <P>&nbsp;</P>. <P>&nbsp;</P>
Warning! Registry member not used
   Text #965: DE&LA-16-550-1-11.pdf. DE&LA-16-550-1-11.pdf. eyJCTSI6MTM5LCJTUCI6LTEsIkVQIjotMSwiU0kiOi0x ...
Warning! Registry member not used
   Text #1437: Calculus Volume 2 - Chapter 5.5 Alternating Series.pdf. Calculus Volume 2 - Chapter 5.5 Alternating Series.pdf ...
Warning! Registry member not used
   Text #1958: 12.01 _Vectors_in_the_Plane.pdf. 12.01 _Vectors_in_the_Plane.pdf. eyJCTSI6MTcwLCJTUCI6LTEs ...
Warning! Registry member not used
   Text #2666: DE&LA-16-550-11-25.pdf. DE&LA-16-550-11-25.pdf. eyJCTSI6MTQwLCJTUCI6LTEsIkVQIjotMSwiU0kiOi ...
6 unused member(s) in the Text registry
Verifying Font registry
Verifying Reference registry
Verifying Ole registry
Verifying Image registry
Warning! Registry member not used
   Image #139: c:\supermemo4\systems\knowledge\temp\PastedImage13525.jpg
Warning! Registry member not used
   Image #143: 2022:06:03 21:17:36: #995 (KNOWLEDGE) (Component 3)
2 unused member(s) in the Image registry
Verifying Sound registry
Verifying Video registry
Verifying Style registry
Verifying Script registry
Verifying Program registry
Warning! Registry member not used
   Binary #31: Logical Equivalences Sheet.pdf
Warning! Registry member not used
   Binary #32: Logical Constants.pdf
Warning! Registry member not used
   Binary #34: Engineering Mechanics_ Statics & Dynamics (2015, Pearson).pdf
Warning! Registry member not used
   Binary #35: Principles of Mathematical Analysis by Walter Rudin.pdf
Warning! Registry member not used
   Binary #48: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-2-3.pdf
Warning! Registry member not used
   Binary #49: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-21.pdf
Warning! Registry member not used
   Binary #50: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-21-23.pdf
Warning! Registry member not used
   Binary #51: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-23-26.pdf
Warning! Registry member not used
   Binary #52: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-26-34.pdf
Warning! Registry member not used
   Binary #53: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-34-39.pdf
Warning! Registry member not used
   Binary #54: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-39-42.pdf
Warning! Registry member not used
   Binary #55: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-42-48.pdf
Warning! Registry member not used
   Binary #56: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-49-59.pdf
Warning! Registry member not used
   Binary #57: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-59-73.pdf
Warning! Registry member not used
   Binary #58: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-73-84.pdf
Warning! Registry member not used
   Binary #59: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-84-100.pdf
Warning! Registry member not used
   Binary #60: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-100-121.pdf
Warning! Registry member not used
   Binary #61: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-121-136.pdf
Warning! Registry member not used
   Binary #62: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-136-154.pdf
Warning! Registry member not used
   Binary #63: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-154-162.pdf
Warning! Registry member not used
   Binary #64: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-162-169.pdf
Warning! Registry member not used
   Binary #65: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-169-175.pdf
Warning! Registry member not used
   Binary #66: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-159-175.pdf
Warning! Registry member not used
   Binary #67: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-175-180.pdf
Warning! Registry member not used
   Binary #68: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-180-192.pdf
Warning! Registry member not used
   Binary #69: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-192-207.pdf
Warning! Registry member not used
   Binary #70: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-207-219.pdf
Warning! Registry member not used
   Binary #71: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-219-232.pdf
Warning! Registry member not used
   Binary #72: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-232-252.pdf
Warning! Registry member not used
   Binary #73: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-252-258.pdf
Warning! Registry member not used
   Binary #74: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-258-277.pdf
Warning! Registry member not used
   Binary #75: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-277-282.pdf
Warning! Registry member not used
   Binary #76: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-283-305.pdf
Warning! Registry member not used
   Binary #77: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-305-314.pdf
Warning! Registry member not used
   Binary #78: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-314-325.pdf
Warning! Registry member not used
   Binary #79: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-325-332.pdf
Warning! Registry member not used
   Binary #80: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-332-342.pdf
Warning! Registry member not used
   Binary #81: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-342-349.pdf
Warning! Registry member not used
   Binary #82: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-349-367.pdf
Warning! Registry member not used
   Binary #83: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-367-378.pdf
Warning! Registry member not used
   Binary #84: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-378-396.pdf
Warning! Registry member not used
   Binary #85: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-396-400.pdf
Warning! Registry member not used
   Binary #86: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-401-417.pdf
Warning! Registry member not used
   Binary #87: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-417-424.pdf
Warning! Registry member not used
   Binary #88: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-424-430.pdf
Warning! Registry member not used
   Binary #89: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-430-438.pdf
Warning! Registry member not used
   Binary #90: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-438-449.pdf
Warning! Registry member not used
   Binary #91: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-449-459.pdf
Warning! Registry member not used
   Binary #92: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-459-461.pdf
Warning! Registry member not used
   Binary #93: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-461-479.pdf
Warning! Registry member not used
   Binary #94: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-479-486.pdf
Warning! Registry member not used
   Binary #95: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-486-498.pdf
Warning! Registry member not used
   Binary #96: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-498-520.pdf
Warning! Registry member not used
   Binary #97: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-520-523.pdf
Warning! Registry member not used
   Binary #98: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-524-528.pdf
Warning! Registry member not used
   Binary #99: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-528-538.pdf
Warning! Registry member not used
   Binary #100: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-538-550.pdf
Warning! Registry member not used
   Binary #101: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-550-560.pdf
Warning! Registry member not used
   Binary #102: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-560-562.pdf
Warning! Registry member not used
   Binary #103: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-562-563.pdf
Warning! Registry member not used
   Binary #104: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-564-577.pdf
Warning! Registry member not used
   Binary #105: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-577-582.pdf
Warning! Registry member not used
   Binary #106: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-582-589.pdf
Warning! Registry member not used
   Binary #107: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-589-599.pdf
Warning! Registry member not used
   Binary #108: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-599-611.pdf
Warning! Registry member not used
   Binary #109: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-611-626.pdf
Warning! Registry member not used
   Binary #110: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-626-633.pdf
Warning! Registry member not used
   Binary #111: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-633-640.pdf
Warning! Registry member not used
   Binary #112: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-640-645.pdf
Warning! Registry member not used
   Binary #113: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-645-661.pdf
Warning! Registry member not used
   Binary #114: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-661-669.pdf
Warning! Registry member not used
   Binary #115: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-669-688.pdf
Warning! Registry member not used
   Binary #116: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-688-699.pdf
Warning! Registry member not used
   Binary #117: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-699-703.pdf
Warning! Registry member not used
   Binary #118: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-704-708.pdf
Warning! Registry member not used
   Binary #119: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-708-713.pdf
Warning! Registry member not used
   Binary #120: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-713-715.pdf
Warning! Registry member not used
   Binary #121: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-715-717.pdf
Warning! Registry member not used
   Binary #122: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-717-723.pdf
Warning! Registry member not used
   Binary #123: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-723-724.pdf
Warning! Registry member not used
   Binary #124: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-724-729.pdf
Warning! Registry member not used
   Binary #125: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-729-732.pdf
Warning! Registry member not used
   Binary #126: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-733-735.pdf
Warning! Registry member not used
   Binary #127: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-735-740.pdf
Warning! Registry member not used
   Binary #128: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-740-745.pdf
Warning! Registry member not used
   Binary #129: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-746-750.pdf
Warning! Registry member not used
   Binary #130: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-750-756.pdf
Warning! Registry member not used
   Binary #131: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-756-760.pdf
Warning! Registry member not used
   Binary #132: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-760-762.pdf
Warning! Registry member not used
   Binary #133: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-762-765.pdf
Warning! Registry member not used
   Binary #134: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-776-794.pdf
Warning! Registry member not used
   Binary #135: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-765-772.pdf
Warning! Registry member not used
   Binary #136: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-772-776.pdf
Warning! Registry member not used
   Binary #137: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-794-797.pdf
Warning! Registry member not used
   Binary #138: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-797-803.pdf
95 unused member(s) in the Program registry
Verifying Template registry
Verifying Tasklist registry
Verifying Concept registry
Verifying Link registry
Deleting directory: c:\users\memory\supermemo\systems\incremental\registry\Text_users
Deleting directory: c:\users\memory\supermemo\systems\incremental\registry\Template_users
Verifying OLE filespace
Verifying image filespace
Verifying sound filespace
Verifying video filespace
Verifying stylesheet filespace
Verifying script filespace
Verifying binary filespace
Verifying text filespace
Inspecting 2159 filespace slots
Unused filespace slot count: 1397
Checking for empty filespace slots
Inspecting 1397 empty filespace slot(s)
Verifying repetition history
Repetition history compressed from 46.8 kB to 46.8 kB
Deleting temporary folders
Deleting directory: c:\users\memory\supermemo\systems\incremental\temp
The size of remaining temporary files is 1.167 MB
   These files can be deleted with File : Tools : Garbage
Removing empty collection directories

Process completed at 12:12:01 PM in 00:00:10 sec (Saturday, November 5, 2022)

NO ERRORS (Checking the integrity of the collection)

____________________________________________________________
