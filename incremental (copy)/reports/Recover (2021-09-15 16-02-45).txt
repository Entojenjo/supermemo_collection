SuperMemo Report
   Activity: Checking the integrity of the collection
   Collection: c:\supermemo\systems\math
   Date: Wednesday, September 15, 2021, 4:02:45 PM
   SuperMemo 18 (Build 18.05, Oct 29, 2020)

Checking 15 element records (ElementInfo.dat)
Turning on tasklist verification along element data integrity checks
Verifying integrity of element data (15 elements in ElementInfo.dat)
Verifying knowledge tree (15 elements in Contents.dat)
Verifying the priority queue of 14 elements
Verifying repetition schedule (Workload.dat and ElementInfo.dat)
Verifying pending queue (Intact.dat)
Verifying A-Factor, Lapses and Repetitions distributions
Recovering registries and filespace references
Rebuilding Text registry (33 members in a 38 member file)
Rebuilding Template registry (17 members in a 17 member file)
Rebuilding Font registry (0 members in a 0 member file)
Rebuilding Reference registry (0 members in a 0 member file)
Rebuilding Style registry (0 members in a 0 member file)
Rebuilding Script registry (0 members in a 0 member file)
Rebuilding Program registry (0 members in a 0 member file)
Rebuilding Ole registry (0 members in a 0 member file)
Rebuilding Video registry (0 members in a 0 member file)
Rebuilding Image registry (7 members in a 7 member file)
Rebuilding Sound registry (0 members in a 0 member file)
Rebuilding Tasklist registry (1 members in a 1 member file)
Rebuilding Concept registry (1 members in a 1 member file)
Rebuilding Link registry (0 members in a 0 member file)
Registering components
Transferring templates
Registering references
Registering image references
Registration of links for 0 members in a 0 member file
Registration of concepts for 1 members in a 1 member file
Verifying Text registry
Warning! Registry member not used
   Text #3: HTML: <P>&nbsp;</P>
1 unused member(s) in the Text registry
Verifying Font registry
Verifying Reference registry
Verifying Ole registry
Verifying Image registry
Verifying Sound registry
Verifying Video registry
Verifying Style registry
Verifying Script registry
Verifying Program registry
Verifying Template registry
Verifying Tasklist registry
Warning! Registry member not used
   Tasklist #1: Tasks
1 unused member(s) in the Tasklist registry
Verifying Concept registry
Verifying Link registry
Verifying OLE filespace
Verifying image filespace
Verifying sound filespace
Verifying video filespace
Verifying stylesheet filespace
Verifying script filespace
Verifying binary filespace
Verifying text filespace
Inspecting 26 filespace slots
Unused filespace slot count: 2
Checking for empty filespace slots
Inspecting 2 empty filespace slot(s)
Building new lexicon: 38 texts
Warning!
   Changed registry size
   Registry:      Lexicon
   Size reported:    52
   Size found:  45
Verifying repetition history
Repetition history compressed from 1287 bytes to 1248 bytes
Deleting temporary folders
Deleting directory: c:\supermemo\systems\math\temp
Removing empty collection directories
Deleting directory: c:\supermemo\systems\math\subsets\

Process completed at 4:02:46 PM in 00:00:00 sec (Wednesday, September 15, 2021)

NO ERRORS (Checking the integrity of the collection)

____________________________________________________________
