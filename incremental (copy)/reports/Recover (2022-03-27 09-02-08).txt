SuperMemo Report
   Activity: Checking the integrity of the collection
   Collection: c:\supermemo\systems\sciences
   Date: Sunday, March 27, 2022, 9:02:08 AM
   SuperMemo 18 (Build 18.05, Oct 29, 2020)

Checking 630 element records (ElementInfo.dat)
Turning on tasklist verification along element data integrity checks
Verifying integrity of element data (630 elements in ElementInfo.dat)
++++++++++++++++++++++    ERROR #1   ++++++++++++++++++++++
   Non-zero parameters in a dismissed item #621
Verifying knowledge tree (630 elements in Contents.dat)
Verifying the priority queue of 616 elements
Verifying repetition schedule (Workload.dat and ElementInfo.dat)
Verifying pending queue (Intact.dat)
Verifying A-Factor, Lapses and Repetitions distributions
Recovering registries and filespace references
Rebuilding Text registry (1232 members in a 1438 member file)
Rebuilding Template registry (21 members in a 22 member file)
Rebuilding Font registry (0 members in a 0 member file)
Rebuilding Reference registry (2 members in a 2 member file)
Rebuilding Style registry (0 members in a 0 member file)
Rebuilding Script registry (0 members in a 0 member file)
Rebuilding Program registry (2 members in a 2 member file)
Rebuilding Ole registry (0 members in a 0 member file)
Rebuilding Video registry (0 members in a 0 member file)
Rebuilding Image registry (89 members in a 91 member file)
Rebuilding Sound registry (0 members in a 0 member file)
Rebuilding Tasklist registry (1 members in a 1 member file)
Rebuilding Concept registry (18 members in a 18 member file)
Rebuilding Link registry (7 members in a 7 member file)
Registering components
Transferring templates
Registering references
Registering image references
Registration of links for 7 members in a 7 member file
Registration of concepts for 18 members in a 18 member file
Verifying Text registry
Warning! Registry member not used
   Text #3: HTML: <P>&nbsp;</P>
Warning! Registry member not used
   Text #100: HTML: <P>&nbsp;</P>. <P>&nbsp;</P>
2 unused member(s) in the Text registry
Verifying Font registry
Verifying Reference registry
Verifying Ole registry
Verifying Image registry
Verifying Sound registry
Verifying Video registry
Verifying Style registry
Verifying Script registry
Verifying Program registry
Warning! Registry member not used
   Binary #1: Aerokashi.pdf
Warning! Registry member not used
   Binary #2: Calc2-Sequences.pdf
2 unused member(s) in the Program registry
Verifying Template registry
Verifying Tasklist registry
Verifying Concept registry
Verifying Link registry
Verifying OLE filespace
Verifying image filespace
Verifying sound filespace
Verifying video filespace
Verifying stylesheet filespace
Verifying script filespace
Verifying binary filespace
Verifying text filespace
Inspecting 559 filespace slots
Unused filespace slot count: 2
Checking for empty filespace slots
Inspecting 2 empty filespace slot(s)
Verifying repetition history
Repetition history compressed from 78.429 kB to 77.961 kB
Deleting temporary folders
The size of remaining temporary files is 138.318 kB
   These files can be deleted with File : Tools : Garbage
Removing empty collection directories
Deleting directory: c:\supermemo\systems\sciences\sma\configs\SuperMemoAssistant.Plugins.Dictionary\
Deleting directory: c:\supermemo\systems\sciences\sma\configs\SuperMemoAssistant.Plugins.LaTeX\
Deleting directory: c:\supermemo\systems\sciences\sma\configs\SuperMemoAssistant.Plugins.PDF\

Process completed at 9:02:13 AM in 00:00:05 sec (Sunday, March 27, 2022)

ONE ERROR (Checking the integrity of the collection)

____________________________________________________________
