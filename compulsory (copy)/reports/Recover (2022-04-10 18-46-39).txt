SuperMemo Report
   Activity: Checking the integrity of the collection
   Collection: c:\supermemo\systems\sciences
   Date: Sunday, April 10, 2022, 6:46:39 PM
   SuperMemo 18 (Build 18.05, Oct 29, 2020)

Checking 717 element records (ElementInfo.dat)
Turning on tasklist verification along element data integrity checks
Verifying integrity of element data (717 elements in ElementInfo.dat)
++++++++++++++++++++++    ERROR #1   ++++++++++++++++++++++
   Non-zero parameters in a dismissed item #694
++++++++++++++++++++++    ERROR #2   ++++++++++++++++++++++
   Non-zero parameters in a dismissed item #699
++++++++++++++++++++++    ERROR #3   ++++++++++++++++++++++
   Non-zero parameters in a dismissed item #701
Verifying knowledge tree (717 elements in Contents.dat)
Verifying the priority queue of 696 elements
Verifying repetition schedule (Workload.dat and ElementInfo.dat)
Verifying pending queue (Intact.dat)
Verifying A-Factor, Lapses and Repetitions distributions
Recovering registries and filespace references
Rebuilding Text registry (1390 members in a 1716 member file)
Rebuilding Template registry (24 members in a 25 member file)
Rebuilding Font registry (0 members in a 0 member file)
Rebuilding Reference registry (15 members in a 15 member file)
Rebuilding Style registry (0 members in a 0 member file)
Rebuilding Script registry (0 members in a 0 member file)
Rebuilding Program registry (15 members in a 15 member file)
Rebuilding Ole registry (0 members in a 0 member file)
Rebuilding Video registry (0 members in a 0 member file)
Rebuilding Image registry (115 members in a 118 member file)
Rebuilding Sound registry (0 members in a 0 member file)
Rebuilding Tasklist registry (1 members in a 1 member file)
Rebuilding Concept registry (22 members in a 22 member file)
Rebuilding Link registry (7 members in a 7 member file)
Registering components
Transferring templates
Registering references
Registering image references
Registration of links for 7 members in a 7 member file
Registration of concepts for 22 members in a 22 member file
Verifying Text registry
Warning! Registry member not used
   Text #3: HTML: <P>&nbsp;</P>
Warning! Registry member not used
   Text #100: HTML: <P>&nbsp;</P>. <P>&nbsp;</P>
Warning! Registry member not used
   Text #1437: Calculus Volume 2 - Chapter 5.5 Alternating Series.pdf. Calculus Volume 2 - Chapter 5.5 Alternating Series.pdf ...
3 unused member(s) in the Text registry
Verifying Font registry
Verifying Reference registry
Verifying Ole registry
Verifying Image registry
Verifying Sound registry
Verifying Video registry
Verifying Style registry
Verifying Script registry
Verifying Program registry
Warning! Registry member not used
   Binary #1: Aerokashi.pdf
Warning! Registry member not used
   Binary #2: Calc2-Sequences.pdf
Warning! Registry member not used
   Binary #3: Calculus Volume 2 - Chapter 5.2 Series.pdf
Warning! Registry member not used
   Binary #4: Calculus Volume 2 - Chapter 5.4 Comparison Tests.pdf
Warning! Registry member not used
   Binary #5: Calculus Volume 2 - Chapter 5.3 The Divergence and Integral Tests.pdf
Warning! Registry member not used
   Binary #6: Calculus Volume 2 - Chapter 5.5 Alternating Series.pdf
Warning! Registry member not used
   Binary #7: University Physics Volume 1 - Chapter 5.1 Forces.pdf
Warning! Registry member not used
   Binary #8: University Physics Volume 1 - Chapter 5.2 Newton's First Law.pdf
Warning! Registry member not used
   Binary #9: University Physics Volume 1 - Chapter 5.3 Newton's Second Law.pdf
Warning! Registry member not used
   Binary #10: University Physics Volume 1 - Chapter 5.4 Mass and Weight.pdf
Warning! Registry member not used
   Binary #11: University Physics Volume 1 - Chapter 5.5 Newton's Third Law.pdf
Warning! Registry member not used
   Binary #12: University Physics Volume 1 - Chapter 5.6 Common Forces.pdf
Warning! Registry member not used
   Binary #13: University Physics Volume 1 - Chapter 5.7 Drawing Free-Body Diagrams.pdf
Warning! Registry member not used
   Binary #14: Calculus Volume 2 - Chapter 5.6 Ratio and Root Tests.pdf
Warning! Registry member not used
   Binary #15: University Physics Volume 1 - Chapter 6.1 - Solving Problems with Newton's Laws.pdf
15 unused member(s) in the Program registry
Verifying Template registry
Verifying Tasklist registry
Verifying Concept registry
Verifying Link registry
Deleting directory: c:\supermemo\systems\sciences\registry\Template_users
Verifying OLE filespace
Verifying image filespace
Verifying sound filespace
Verifying video filespace
Verifying stylesheet filespace
Verifying script filespace
Verifying binary filespace
Verifying text filespace
Inspecting 668 filespace slots
Unused filespace slot count: 2
Checking for empty filespace slots
Inspecting 2 empty filespace slot(s)
Deleting directory: c:\supermemo\systems\sciences\registry\lexicon_users
Building new lexicon: 1,716 texts
Warning!
   Changed registry size
   Registry:      Lexicon
   Size reported:    1309
   Size found:  1695
Verifying repetition history
Repetition history compressed from 84.435 kB to 83.85 kB
Deleting temporary folders
Deleting directory: c:\supermemo\systems\sciences\temp
The size of remaining temporary files is 173.197 kB
   These files can be deleted with File : Tools : Garbage
Removing empty collection directories
Deleting directory: c:\supermemo\systems\sciences\sma\configs\SuperMemoAssistant.Plugins.Dictionary\
Deleting directory: c:\supermemo\systems\sciences\sma\configs\SuperMemoAssistant.Plugins.LaTeX\
Deleting directory: c:\supermemo\systems\sciences\sma\configs\SuperMemoAssistant.Plugins.PDF\

Process completed at 6:46:58 PM in 00:00:19 sec (Sunday, April 10, 2022)

3 ERRORS (Checking the integrity of the collection)

____________________________________________________________
