SuperMemo Report
   Activity: Checking the integrity of the collection
   Collection: c:\users\memory\supermemo\systems\knowledge
   Date: Saturday, September 24, 2022, 6:31:55 PM
   SuperMemo 18 (Build 18.05, Oct 29, 2020)

Checking 1,758 element records (ElementInfo.dat)
Turning on tasklist verification along element data integrity checks
Verifying integrity of element data (1758 elements in ElementInfo.dat)
Verifying knowledge tree (1,758 elements in Contents.dat)
136 children at Topic #1,320: The OpenXR Specification Copyright (c) 2017-2022
Verifying the priority queue of 1,655 elements
Verifying repetition schedule (Workload.dat and ElementInfo.dat)
Verifying pending queue (Intact.dat)
Verifying A-Factor, Lapses and Repetitions distributions
Recovering registries and filespace references
Rebuilding Text registry (3715 members in a 4300 member file)
Rebuilding Template registry (24 members in a 26 member file)
Rebuilding Font registry (0 members in a 0 member file)
Rebuilding Reference registry (252 members in a 253 member file)
Rebuilding Style registry (0 members in a 0 member file)
Rebuilding Script registry (0 members in a 0 member file)
Rebuilding Program registry (223 members in a 223 member file)
Rebuilding Ole registry (0 members in a 0 member file)
Rebuilding Video registry (0 members in a 0 member file)
Rebuilding Image registry (118 members in a 147 member file)
Rebuilding Sound registry (0 members in a 0 member file)
Rebuilding Tasklist registry (1 members in a 3 member file)
Rebuilding Concept registry (60 members in a 60 member file)
Rebuilding Link registry (7 members in a 9 member file)
Registering components
Transferring templates
Registering references
Registering image references
Registration of links for 7 members in a 9 member file
Registration of concepts for 60 members in a 60 member file
Verifying Text registry
Warning! Registry member not used
   Text #3: HTML: <P>&nbsp;</P>
Warning! Registry member not used
   Text #100: HTML: <P>&nbsp;</P>. <P>&nbsp;</P>
Warning! Registry member not used
   Text #965: DE&LA-16-550-1-11.pdf. DE&LA-16-550-1-11.pdf. eyJCTSI6MTM5LCJTUCI6LTEsIkVQIjotMSwiU0kiOi0x ...
Warning! Registry member not used
   Text #1437: Calculus Volume 2 - Chapter 5.5 Alternating Series.pdf. Calculus Volume 2 - Chapter 5.5 Alternating Series.pdf ...
Warning! Registry member not used
   Text #1958: 12.01 _Vectors_in_the_Plane.pdf. 12.01 _Vectors_in_the_Plane.pdf. eyJCTSI6MTcwLCJTUCI6LTEs ...
Warning! Registry member not used
   Text #2666: DE&LA-16-550-11-25.pdf. DE&LA-16-550-11-25.pdf. eyJCTSI6MTQwLCJTUCI6LTEsIkVQIjotMSwiU0kiOi ...
6 unused member(s) in the Text registry
Verifying Font registry
Verifying Reference registry
Verifying Ole registry
Verifying Image registry
Warning! Registry member not used
   Image #139: c:\supermemo4\systems\knowledge\temp\PastedImage13525.jpg
Warning! Registry member not used
   Image #143: 2022:06:03 21:17:36: #995 (KNOWLEDGE) (Component 3)
2 unused member(s) in the Image registry
Verifying Sound registry
Verifying Video registry
Verifying Style registry
Verifying Script registry
Verifying Program registry
Warning! Registry member not used
   Binary #1: Aerokashi.pdf
Warning! Registry member not used
   Binary #2: Calc2-Sequences.pdf
Warning! Registry member not used
   Binary #3: Calculus Volume 2 - Chapter 5.2 Series.pdf
Warning! Registry member not used
   Binary #4: Calculus Volume 2 - Chapter 5.4 Comparison Tests.pdf
Warning! Registry member not used
   Binary #5: Calculus Volume 2 - Chapter 5.3 The Divergence and Integral Tests.pdf
Warning! Registry member not used
   Binary #6: Calculus Volume 2 - Chapter 5.5 Alternating Series.pdf
Warning! Registry member not used
   Binary #7: University Physics Volume 1 - Chapter 5.1 Forces.pdf
Warning! Registry member not used
   Binary #8: University Physics Volume 1 - Chapter 5.2 Newton's First Law.pdf
Warning! Registry member not used
   Binary #9: University Physics Volume 1 - Chapter 5.3 Newton's Second Law.pdf
Warning! Registry member not used
   Binary #10: University Physics Volume 1 - Chapter 5.4 Mass and Weight.pdf
Warning! Registry member not used
   Binary #11: University Physics Volume 1 - Chapter 5.5 Newton's Third Law.pdf
Warning! Registry member not used
   Binary #12: University Physics Volume 1 - Chapter 5.6 Common Forces.pdf
Warning! Registry member not used
   Binary #13: University Physics Volume 1 - Chapter 5.7 Drawing Free-Body Diagrams.pdf
Warning! Registry member not used
   Binary #14: Calculus Volume 2 - Chapter 5.6 Ratio and Root Tests.pdf
Warning! Registry member not used
   Binary #15: University Physics Volume 1 - Chapter 6.1 - Solving Problems with Newton's Laws.pdf
Warning! Registry member not used
   Binary #16: University Physics Volume 1 - Chapter 6.2 - Friction.pdf
Warning! Registry member not used
   Binary #17: Calculus Volume 2 - Chapter 5 Review + Questions.pdf
Warning! Registry member not used
   Binary #18: Calculus Volume 2 - Chapter 6.1 Power Series.pdf
Warning! Registry member not used
   Binary #19: Calculus Volume 2 - Chapter 6.2 Properties of Power Series.pdf
Warning! Registry member not used
   Binary #20: Calculus Volume 2 - Chapter 6.3 Taylor and Maclaurin Series.pdf
Warning! Registry member not used
   Binary #21: Calculus Volume 2 - Chapter 6.4 Working with Taylor Series.pdf
Warning! Registry member not used
   Binary #22: Calculus Volume 2 - Chapter 6 Review + Questions.pdf
Warning! Registry member not used
   Binary #23: University Physics Volume 1 - Chapter 6.3 - Centripetal Force.pdf
Warning! Registry member not used
   Binary #24: University Physics Volume 1 - Chapter 6.4 - Drag Force and Terminal Speed.pdf
Warning! Registry member not used
   Binary #25: University Physics Volume 1 - Chapter 7 - Work.pdf
Warning! Registry member not used
   Binary #26: University Physics Volume 1 - Chapter 7.1 - Work.pdf
Warning! Registry member not used
   Binary #27: University Physics Volume 1 - Chapter 7.2 - Kinetic Energy.pdf
Warning! Registry member not used
   Binary #28: University Physics Volume 1 - Chapter 7.3 - Work-Energy Theorem.pdf
Warning! Registry member not used
   Binary #29: University Physics Volume 1 - Chapter 7.4 - Power.pdf
Warning! Registry member not used
   Binary #30: University Physics Volume 1 - Chapter 7 Review.pdf
Warning! Registry member not used
   Binary #31: Logical Equivalences Sheet.pdf
Warning! Registry member not used
   Binary #32: Logical Constants.pdf
Warning! Registry member not used
   Binary #33: Algebra-and-Trigonometry-2e-WEB.pdf
Warning! Registry member not used
   Binary #34: Engineering Mechanics_ Statics & Dynamics (2015, Pearson).pdf
Warning! Registry member not used
   Binary #35: Principles of Mathematical Analysis by Walter Rudin.pdf
Warning! Registry member not used
   Binary #36: amd64archvol1.pdf
Warning! Registry member not used
   Binary #37: 253666-sdm-vol-2a.pdf
Warning! Registry member not used
   Binary #38: 253665-sdm-vol-1.pdf
Warning! Registry member not used
   Binary #39: 253667-sdm-vol-2b.pdf
Warning! Registry member not used
   Binary #40: 326018-sdm-vol-2c.pdf
Warning! Registry member not used
   Binary #41: 334569-sdm-vol-2d.pdf
Warning! Registry member not used
   Binary #42: 253668-sdm-vol-3a.pdf
Warning! Registry member not used
   Binary #43: 253669-sdm-vol-3b.pdf
Warning! Registry member not used
   Binary #44: 326019-sdm-vol-3c.pdf
Warning! Registry member not used
   Binary #45: 332831-sdm-vol-3d.pdf
Warning! Registry member not used
   Binary #46: 335592-sdm-vol-4.pdf
Warning! Registry member not used
   Binary #47: architecture-instruction-set-extensions-programming-reference.pdf
Warning! Registry member not used
   Binary #48: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-2-3.pdf
Warning! Registry member not used
   Binary #49: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-21.pdf
Warning! Registry member not used
   Binary #50: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-21-23.pdf
Warning! Registry member not used
   Binary #51: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-23-26.pdf
Warning! Registry member not used
   Binary #52: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-26-34.pdf
Warning! Registry member not used
   Binary #53: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-34-39.pdf
Warning! Registry member not used
   Binary #54: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-39-42.pdf
Warning! Registry member not used
   Binary #55: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-42-48.pdf
Warning! Registry member not used
   Binary #56: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-49-59.pdf
Warning! Registry member not used
   Binary #57: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-59-73.pdf
Warning! Registry member not used
   Binary #58: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-73-84.pdf
Warning! Registry member not used
   Binary #59: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-84-100.pdf
Warning! Registry member not used
   Binary #60: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-100-121.pdf
Warning! Registry member not used
   Binary #61: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-121-136.pdf
Warning! Registry member not used
   Binary #62: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-136-154.pdf
Warning! Registry member not used
   Binary #63: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-154-162.pdf
Warning! Registry member not used
   Binary #64: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-162-169.pdf
Warning! Registry member not used
   Binary #65: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-169-175.pdf
Warning! Registry member not used
   Binary #66: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-159-175.pdf
Warning! Registry member not used
   Binary #67: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-175-180.pdf
Warning! Registry member not used
   Binary #68: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-180-192.pdf
Warning! Registry member not used
   Binary #69: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-192-207.pdf
Warning! Registry member not used
   Binary #70: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-207-219.pdf
Warning! Registry member not used
   Binary #71: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-219-232.pdf
Warning! Registry member not used
   Binary #72: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-232-252.pdf
Warning! Registry member not used
   Binary #73: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-252-258.pdf
Warning! Registry member not used
   Binary #74: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-258-277.pdf
Warning! Registry member not used
   Binary #75: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-277-282.pdf
Warning! Registry member not used
   Binary #76: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-283-305.pdf
Warning! Registry member not used
   Binary #77: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-305-314.pdf
Warning! Registry member not used
   Binary #78: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-314-325.pdf
Warning! Registry member not used
   Binary #79: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-325-332.pdf
Warning! Registry member not used
   Binary #80: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-332-342.pdf
Warning! Registry member not used
   Binary #81: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-342-349.pdf
Warning! Registry member not used
   Binary #82: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-349-367.pdf
Warning! Registry member not used
   Binary #83: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-367-378.pdf
Warning! Registry member not used
   Binary #84: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-378-396.pdf
Warning! Registry member not used
   Binary #85: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-396-400.pdf
Warning! Registry member not used
   Binary #86: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-401-417.pdf
Warning! Registry member not used
   Binary #87: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-417-424.pdf
Warning! Registry member not used
   Binary #88: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-424-430.pdf
Warning! Registry member not used
   Binary #89: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-430-438.pdf
Warning! Registry member not used
   Binary #90: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-438-449.pdf
Warning! Registry member not used
   Binary #91: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-449-459.pdf
Warning! Registry member not used
   Binary #92: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-459-461.pdf
Warning! Registry member not used
   Binary #93: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-461-479.pdf
Warning! Registry member not used
   Binary #94: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-479-486.pdf
Warning! Registry member not used
   Binary #95: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-486-498.pdf
Warning! Registry member not used
   Binary #96: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-498-520.pdf
Warning! Registry member not used
   Binary #97: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-520-523.pdf
Warning! Registry member not used
   Binary #98: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-524-528.pdf
Warning! Registry member not used
   Binary #99: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-528-538.pdf
Warning! Registry member not used
   Binary #100: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-538-550.pdf
Warning! Registry member not used
   Binary #101: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-550-560.pdf
Warning! Registry member not used
   Binary #102: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-560-562.pdf
Warning! Registry member not used
   Binary #103: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-562-563.pdf
Warning! Registry member not used
   Binary #104: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-564-577.pdf
Warning! Registry member not used
   Binary #105: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-577-582.pdf
Warning! Registry member not used
   Binary #106: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-582-589.pdf
Warning! Registry member not used
   Binary #107: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-589-599.pdf
Warning! Registry member not used
   Binary #108: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-599-611.pdf
Warning! Registry member not used
   Binary #109: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-611-626.pdf
Warning! Registry member not used
   Binary #110: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-626-633.pdf
Warning! Registry member not used
   Binary #111: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-633-640.pdf
Warning! Registry member not used
   Binary #112: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-640-645.pdf
Warning! Registry member not used
   Binary #113: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-645-661.pdf
Warning! Registry member not used
   Binary #114: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-661-669.pdf
Warning! Registry member not used
   Binary #115: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-669-688.pdf
Warning! Registry member not used
   Binary #116: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-688-699.pdf
Warning! Registry member not used
   Binary #117: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-699-703.pdf
Warning! Registry member not used
   Binary #118: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-704-708.pdf
Warning! Registry member not used
   Binary #119: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-708-713.pdf
Warning! Registry member not used
   Binary #120: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-713-715.pdf
Warning! Registry member not used
   Binary #121: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-715-717.pdf
Warning! Registry member not used
   Binary #122: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-717-723.pdf
Warning! Registry member not used
   Binary #123: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-723-724.pdf
Warning! Registry member not used
   Binary #124: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-724-729.pdf
Warning! Registry member not used
   Binary #125: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-729-732.pdf
Warning! Registry member not used
   Binary #126: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-733-735.pdf
Warning! Registry member not used
   Binary #127: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-735-740.pdf
Warning! Registry member not used
   Binary #128: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-740-745.pdf
Warning! Registry member not used
   Binary #129: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-746-750.pdf
Warning! Registry member not used
   Binary #130: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-750-756.pdf
Warning! Registry member not used
   Binary #131: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-756-760.pdf
Warning! Registry member not used
   Binary #132: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-760-762.pdf
Warning! Registry member not used
   Binary #133: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-762-765.pdf
Warning! Registry member not used
   Binary #134: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-776-794.pdf
Warning! Registry member not used
   Binary #135: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-765-772.pdf
Warning! Registry member not used
   Binary #136: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-772-776.pdf
Warning! Registry member not used
   Binary #137: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-794-797.pdf
Warning! Registry member not used
   Binary #138: Vector Calculus, Linear Algebra, and Differential Forms-A Unified Approach-18-837-797-803.pdf
Warning! Registry member not used
   Binary #139: DE&LA-16-550-1-11.pdf
Warning! Registry member not used
   Binary #140: DE&LA-16-550-11-25.pdf
Warning! Registry member not used
   Binary #141: DE&LA-16-550-25-33.pdf
Warning! Registry member not used
   Binary #142: DE&LA-16-550-33-46.pdf
Warning! Registry member not used
   Binary #143: DE&LA-16-550-46-54.pdf
Warning! Registry member not used
   Binary #144: DE&LA-16-550-55-63.pdf
Warning! Registry member not used
   Binary #145: Tipler_Llewellyn-19-758-3-11.pdf
Warning! Registry member not used
   Binary #146: Tipler_Llewellyn-19-758-11-17.pdf
Warning! Registry member not used
   Binary #147: Tipler_Llewellyn-19-758-17-29.pdf
Warning! Registry member not used
   Binary #148: Tipler_Llewellyn-19-758-29-41.pdf
Warning! Registry member not used
   Binary #149: Tipler_Llewellyn-19-758-41-45.pdf
Warning! Registry member not used
   Binary #150: Tipler_Llewellyn-19-758-45-64.pdf
Warning! Registry member not used
   Binary #151: Tipler_Llewellyn-19-758-65-70.pdf
Warning! Registry member not used
   Binary #152: Tipler_Llewellyn-19-758-70-81.pdf
Warning! Registry member not used
   Binary #153: Tipler_Llewellyn-19-758-84-97.pdf
Warning! Registry member not used
   Binary #154: Tipler_Llewellyn-19-758-81-84.pdf
Warning! Registry member not used
   Binary #155: Tipler_Llewellyn-19-758-97-114.pdf
Warning! Registry member not used
   Binary #156: Tipler_Llewellyn-19-758-115-119.pdf
Warning! Registry member not used
   Binary #157: Tipler_Llewellyn-19-758-119-127.pdf
Warning! Registry member not used
   Binary #158: Tipler_Llewellyn-19-758-127-133.pdf
Warning! Registry member not used
   Binary #159: Tipler_Llewellyn-19-758-133-146.pdf
Warning! Registry member not used
   Binary #160: Tipler_Llewellyn-19-758-147-150.pdf
Warning! Registry member not used
   Binary #161: Tipler_Llewellyn-19-758-150-159.pdf
Warning! Registry member not used
   Binary #162: Tipler_Llewellyn-19-758-159-169.pdf
Warning! Registry member not used
   Binary #163: Tipler_Llewellyn-19-758-169-174.pdf
Warning! Registry member not used
   Binary #164: DE&LA-16-550-63-73.pdf
Warning! Registry member not used
   Binary #165: DE&LA-16-550-73-80.pdf
Warning! Registry member not used
   Binary #166: DE&LA-16-550-80-87.pdf
Warning! Registry member not used
   Binary #167: DE&LA-16-550-87-100.pdf
Warning! Registry member not used
   Binary #168: DE&LA-16-550-100-114.pdf
Warning! Registry member not used
   Binary #169: DE&LA-16-550-115-130.pdf
Warning! Registry member not used
   Binary #170: 12.01 _Vectors_in_the_Plane.pdf
Warning! Registry member not used
   Binary #171: 12.1E Exercises for Section 12.1 - Mathematics LibreTexts.pdf
Warning! Registry member not used
   Binary #172: 12.1E _Exercises_for_Section_12.1.pdf
Warning! Registry member not used
   Binary #173: 12.02 _Vectors_in_Three_Dimensions.pdf
Warning! Registry member not used
   Binary #174: 12.2E _Exercises_for_Section_12.2.pdf
Warning! Registry member not used
   Binary #175: 12.03 _The_Dot_Product.pdf
Warning! Registry member not used
   Binary #176: 12.3E _Exercises_for_Section_12.3.pdf
Warning! Registry member not used
   Binary #177: 12.04 _The_Cross_Product.pdf
Warning! Registry member not used
   Binary #178: 12.4E _Exercises_for_Section_12.4.pdf
Warning! Registry member not used
   Binary #179: 12.05 _Equations_of_Lines_and_Planes_in_Space.pdf
Warning! Registry member not used
   Binary #180: 12.5E _Exercises_for_Section_12.5.pdf
Warning! Registry member not used
   Binary #181: 12.06 _Quadric_Surfaces.pdf
Warning! Registry member not used
   Binary #182: 12.6E _Exercises_for_Section_12.6.pdf
Warning! Registry member not used
   Binary #183: 12.07 _Cylindrical_and_Spherical_Coordinates.pdf
Warning! Registry member not used
   Binary #184: 12.7E _Exercises_for_Section_12.7.pdf
Warning! Registry member not used
   Binary #185: Tipler_Llewellyn-19-758-174-184.pdf
Warning! Registry member not used
   Binary #186: Tipler_Llewellyn-19-758-185-187.pdf
Warning! Registry member not used
   Binary #187: 13.01__Vector-Valued_Functions_and_Space_Curves.pdf
Warning! Registry member not used
   Binary #188: 13.1E__Exercises_for_Section_13.1.pdf
Warning! Registry member not used
   Binary #189: 13.02__Calculus_of_Vector-Valued_Functions.pdf
Warning! Registry member not used
   Binary #190: 13.2E__Exercises_for_Section_13.2.pdf
Warning! Registry member not used
   Binary #191: 13.03__Arc_Length_and_Curvature.pdf
Warning! Registry member not used
   Binary #192: 13.3E__Exercises_for_Section_13.3.pdf
Warning! Registry member not used
   Binary #193: 13.04__Motion_in_Space.pdf
Warning! Registry member not used
   Binary #194: 13.4E__Exercises_for_Section_13.4.pdf
Warning! Registry member not used
   Binary #195: 13.05__Chapter_13_Review_Exercises.pdf
Warning! Registry member not used
   Binary #196: 12.08__Chapter_12_Review_Exercises.pdf
Warning! Registry member not used
   Binary #197: Outcomes, Events, and Probability.pdf
Warning! Registry member not used
   Binary #198: Conditional Probability and Independence.pdf
Warning! Registry member not used
   Binary #199: Discrete Random Variables.pdf
Warning! Registry member not used
   Binary #200: Continuous Random Variables.pdf
Warning! Registry member not used
   Binary #201: Simulation.pdf
Warning! Registry member not used
   Binary #202: Expectation and Variance.pdf
Warning! Registry member not used
   Binary #203: Computations with Random Variables.pdf
Warning! Registry member not used
   Binary #204: Joint Distributions and Independence.pdf
Warning! Registry member not used
   Binary #205: Covariance and Correlation.pdf
Warning! Registry member not used
   Binary #206: More Computations with More Random Variables.pdf
Warning! Registry member not used
   Binary #207: The Poisson Process.pdf
Warning! Registry member not used
   Binary #208: The Law of Large Numbers.pdf
Warning! Registry member not used
   Binary #209: The Central Limit Theorem.pdf
Warning! Registry member not used
   Binary #210: Exploratory Data Analysis - Graphical Summaries.pdf
Warning! Registry member not used
   Binary #211: Exploratory Data Analysis - Numerical Summaries.pdf
Warning! Registry member not used
   Binary #212: Basic Statistical Models.pdf
Warning! Registry member not used
   Binary #213: The Bootstrap.pdf
Warning! Registry member not used
   Binary #214: Unbiased Estimators.pdf
Warning! Registry member not used
   Binary #215: Efficiency and mean squared error.pdf
Warning! Registry member not used
   Binary #216: Maximum Likelihood.pdf
Warning! Registry member not used
   Binary #217: The Method of Least Squares.pdf
Warning! Registry member not used
   Binary #218: Confidence Intervals for the Mean.pdf
Warning! Registry member not used
   Binary #219: More on Confidence Intervals.pdf
Warning! Registry member not used
   Binary #220: Testing Hypotheses - Essentials.pdf
Warning! Registry member not used
   Binary #221: Testing Hypotheses - Elaboration.pdf
Warning! Registry member not used
   Binary #222: The t-Test.pdf
Warning! Registry member not used
   Binary #223: Comparing Two Samples.pdf
223 unused member(s) in the Program registry
Verifying Template registry
Verifying Tasklist registry
Verifying Concept registry
Verifying Link registry
Deleting directory: c:\users\memory\supermemo\systems\knowledge\registry\Text_users
Deleting directory: c:\users\memory\supermemo\systems\knowledge\registry\Template_users
Verifying OLE filespace
Verifying image filespace
Verifying sound filespace
Verifying video filespace
Verifying stylesheet filespace
Verifying script filespace
Verifying binary filespace
Verifying text filespace
Inspecting 2054 filespace slots
Unused filespace slot count: 19
Checking for empty filespace slots
Inspecting 19 empty filespace slot(s)
Deleting directory: c:\users\memory\supermemo\systems\knowledge\registry\lexicon_users
Building new lexicon: 4,300 texts
Warning!
   Changed registry size
   Registry:      Lexicon
   Size reported:    24823
   Size found:  26348
Verifying repetition history
Repetition history compressed from 159.744 kB to 159.744 kB
Deleting temporary folders
Deleting directory: c:\users\memory\supermemo\systems\knowledge\temp
The size of remaining temporary files is 930.072 kB
   These files can be deleted with File : Tools : Garbage
Removing empty collection directories

Process completed at 6:37:58 PM in 00:06:02 sec (Saturday, September 24, 2022)

NO ERRORS (Checking the integrity of the collection)

____________________________________________________________
