SuperMemo Report
   Activity: Checking the integrity of the collection
   Collection: c:\truememo\systems\knowledge
   Date: Friday, June 3, 2022, 9:58:21 PM
   SuperMemo 18 (Build 18.05, Oct 29, 2020)

Checking 995 element records (ElementInfo.dat)
Turning on tasklist verification along element data integrity checks
Verifying integrity of element data (995 elements in ElementInfo.dat)
++++++++++++++++++++++    ERROR #1   ++++++++++++++++++++++
   Non-zero parameters in a dismissed item #961
++++++++++++++++++++++    ERROR #2   ++++++++++++++++++++++
   Non-zero parameters in a dismissed item #992
Verifying knowledge tree (995 elements in Contents.dat)
Verifying the priority queue of 966 elements
Verifying repetition schedule (Workload.dat and ElementInfo.dat)
Verifying pending queue (Intact.dat)
Verifying A-Factor, Lapses and Repetitions distributions
Recovering registries and filespace references
Rebuilding Text registry (1997 members in a 2295 member file)
Rebuilding Template registry (23 members in a 25 member file)
Rebuilding Font registry (0 members in a 0 member file)
Rebuilding Reference registry (50 members in a 52 member file)
Rebuilding Style registry (0 members in a 0 member file)
Rebuilding Script registry (0 members in a 0 member file)
Rebuilding Program registry (31 members in a 31 member file)
Rebuilding Ole registry (0 members in a 0 member file)
Rebuilding Video registry (0 members in a 0 member file)
Rebuilding Image registry (141 members in a 143 member file)
Rebuilding Sound registry (0 members in a 0 member file)
Rebuilding Tasklist registry (3 members in a 3 member file)
Rebuilding Concept registry (31 members in a 32 member file)
Rebuilding Link registry (9 members in a 9 member file)
Registering components
Transferring templates
Registering references
Registering image references
Registration of links for 9 members in a 9 member file
Registration of concepts for 31 members in a 32 member file
Verifying Text registry
Warning! Registry member not used
   Text #3: HTML: <P>&nbsp;</P>
Warning! Registry member not used
   Text #100: HTML: <P>&nbsp;</P>. <P>&nbsp;</P>
Warning! Registry member not used
   Text #1437: Calculus Volume 2 - Chapter 5.5 Alternating Series.pdf. Calculus Volume 2 - Chapter 5.5 Alternating Series.pdf ...
3 unused member(s) in the Text registry
Verifying Font registry
Verifying Reference registry
Verifying Ole registry
Verifying Image registry
Warning! Registry member not used
   Image #139: c:\supermemo4\systems\knowledge\temp\PastedImage13525.jpg
Warning! Registry member not used
   Image #143: 2022:06:03 21:17:36: #995 (KNOWLEDGE) (Component 3)
2 unused member(s) in the Image registry
Verifying Sound registry
Verifying Video registry
Verifying Style registry
Verifying Script registry
Verifying Program registry
Warning! Registry member not used
   Binary #1: Aerokashi.pdf
Warning! Registry member not used
   Binary #2: Calc2-Sequences.pdf
Warning! Registry member not used
   Binary #3: Calculus Volume 2 - Chapter 5.2 Series.pdf
Warning! Registry member not used
   Binary #4: Calculus Volume 2 - Chapter 5.4 Comparison Tests.pdf
Warning! Registry member not used
   Binary #5: Calculus Volume 2 - Chapter 5.3 The Divergence and Integral Tests.pdf
Warning! Registry member not used
   Binary #6: Calculus Volume 2 - Chapter 5.5 Alternating Series.pdf
Warning! Registry member not used
   Binary #7: University Physics Volume 1 - Chapter 5.1 Forces.pdf
Warning! Registry member not used
   Binary #8: University Physics Volume 1 - Chapter 5.2 Newton's First Law.pdf
Warning! Registry member not used
   Binary #9: University Physics Volume 1 - Chapter 5.3 Newton's Second Law.pdf
Warning! Registry member not used
   Binary #10: University Physics Volume 1 - Chapter 5.4 Mass and Weight.pdf
Warning! Registry member not used
   Binary #11: University Physics Volume 1 - Chapter 5.5 Newton's Third Law.pdf
Warning! Registry member not used
   Binary #12: University Physics Volume 1 - Chapter 5.6 Common Forces.pdf
Warning! Registry member not used
   Binary #13: University Physics Volume 1 - Chapter 5.7 Drawing Free-Body Diagrams.pdf
Warning! Registry member not used
   Binary #14: Calculus Volume 2 - Chapter 5.6 Ratio and Root Tests.pdf
Warning! Registry member not used
   Binary #15: University Physics Volume 1 - Chapter 6.1 - Solving Problems with Newton's Laws.pdf
Warning! Registry member not used
   Binary #16: University Physics Volume 1 - Chapter 6.2 - Friction.pdf
Warning! Registry member not used
   Binary #17: Calculus Volume 2 - Chapter 5 Review + Questions.pdf
Warning! Registry member not used
   Binary #18: Calculus Volume 2 - Chapter 6.1 Power Series.pdf
Warning! Registry member not used
   Binary #19: Calculus Volume 2 - Chapter 6.2 Properties of Power Series.pdf
Warning! Registry member not used
   Binary #20: Calculus Volume 2 - Chapter 6.3 Taylor and Maclaurin Series.pdf
Warning! Registry member not used
   Binary #21: Calculus Volume 2 - Chapter 6.4 Working with Taylor Series.pdf
Warning! Registry member not used
   Binary #22: Calculus Volume 2 - Chapter 6 Review + Questions.pdf
Warning! Registry member not used
   Binary #23: University Physics Volume 1 - Chapter 6.3 - Centripetal Force.pdf
Warning! Registry member not used
   Binary #24: University Physics Volume 1 - Chapter 6.4 - Drag Force and Terminal Speed.pdf
Warning! Registry member not used
   Binary #25: University Physics Volume 1 - Chapter 7 - Work.pdf
Warning! Registry member not used
   Binary #26: University Physics Volume 1 - Chapter 7.1 - Work.pdf
Warning! Registry member not used
   Binary #27: University Physics Volume 1 - Chapter 7.2 - Kinetic Energy.pdf
Warning! Registry member not used
   Binary #28: University Physics Volume 1 - Chapter 7.3 - Work-Energy Theorem.pdf
Warning! Registry member not used
   Binary #29: University Physics Volume 1 - Chapter 7.4 - Power.pdf
Warning! Registry member not used
   Binary #30: University Physics Volume 1 - Chapter 7 Review.pdf
Warning! Registry member not used
   Binary #31: Logical Equivalences Sheet.pdf
31 unused member(s) in the Program registry
Verifying Template registry
Verifying Tasklist registry
Warning! Registry member not used
   Tasklist #1: Tasks
Warning! Registry member not used
   Tasklist #2: General
Warning! Registry member not used
   Tasklist #3: Summer Goals
3 unused member(s) in the Tasklist registry
Verifying Concept registry
Verifying Link registry
Deleting directory: c:\truememo\systems\knowledge\registry\Template_users
Verifying OLE filespace
Verifying image filespace
Verifying sound filespace
Verifying video filespace
Verifying stylesheet filespace
Verifying script filespace
Verifying binary filespace
Verifying text filespace
Warning! Removing formatting from member #1243:
   Text: Definition of Presupposes
   Format: HTML
Inspecting 1030 filespace slots
Unused filespace slot count: 3
Checking for empty filespace slots
Inspecting 3 empty filespace slot(s)
Formatting removed from 1 texts
   Including 0 RTF text(s) and 1 HTML text(s)
Deleting directory: c:\truememo\systems\knowledge\registry\lexicon_users
Building new lexicon: 2,295 texts
Warning!
   Changed registry size
   Registry:      Lexicon
   Size reported:    10933
   Size found:  13175
Verifying repetition history
++++++++++++++++++++++    ERROR #3   ++++++++++++++++++++++
   Registering a review in the timeline: 
   Jun 03, 2022, 20:55:21
   Topic #557: Propositional calculus
++++++++++++++++++++++    ERROR #4   ++++++++++++++++++++++
   Registering a review in the timeline: 
   Jun 03, 2022, 19:18:17
   Topic #600: Tasklist Manager
++++++++++++++++++++++    ERROR #5   ++++++++++++++++++++++
   Registering a review in the timeline: 
   Jun 03, 2022, 20:04:08
   Topic #837: Options
++++++++++++++++++++++    ERROR #6   ++++++++++++++++++++++
   Registering a review in the timeline: 
   Jun 03, 2022, 18:36:07
   Topic #907: Priority Queue
++++++++++++++++++++++    ERROR #7   ++++++++++++++++++++++
   Registering a review in the timeline: 
   Jun 03, 2022, 18:25:53
   Topic #979: Regularly inspect Toolkit : Statistics : Analysis : Use : Priority protection : Items (Shift+Alt+A opens this tab as you left it last time). Priority protection is the most .
++++++++++++++++++++++    ERROR #8   ++++++++++++++++++++++
   Registering a review in the timeline: 
   Jun 03, 2022, 19:07:04
   Item #983: A sequence    is increasing (for all ) if [.](what must the sequence be doing?)
++++++++++++++++++++++    ERROR #9   ++++++++++++++++++++++
   Registering a review in the timeline: 
   Jun 03, 2022, 19:53:16
   Item #985: Problem Solving Strategy for Choosing a Convergence Test
Repetition history compressed from 118.287 kB to 117.234 kB
Deleting temporary folders
Deleting directory: c:\truememo\systems\knowledge\temp
The size of remaining temporary files is 376.558 kB
   These files can be deleted with File : Tools : Garbage
Removing empty collection directories
Deleting directory: c:\truememo\systems\knowledge\sma\configs\SuperMemoAssistant.Plugins.Dictionary\
Deleting directory: c:\truememo\systems\knowledge\sma\configs\SuperMemoAssistant.Plugins.LaTeX\
Deleting directory: c:\truememo\systems\knowledge\sma\configs\SuperMemoAssistant.Plugins.PDF\

Process completed at 9:59:57 PM in 00:01:35 sec (Friday, June 3, 2022)

9 ERRORS (Checking the integrity of the collection)

____________________________________________________________
