SuperMemo Report
   Activity: Checking the integrity of the collection
   Collection: c:\supermemo\systems\math
   Date: Friday, November 26, 2021, 2:54:18 PM
   SuperMemo 18 (Build 18.05, Oct 29, 2020)

Checking 256 element records (ElementInfo.dat)
Turning on tasklist verification along element data integrity checks
Verifying integrity of element data (256 elements in ElementInfo.dat)
Verifying knowledge tree (256 elements in Contents.dat)
Verifying the priority queue of 249 elements
Verifying repetition schedule (Workload.dat and ElementInfo.dat)
Verifying pending queue (Intact.dat)
Verifying A-Factor, Lapses and Repetitions distributions
Recovering registries and filespace references
Rebuilding Text registry (467 members in a 497 member file)
Rebuilding Template registry (20 members in a 21 member file)
Rebuilding Font registry (0 members in a 0 member file)
Rebuilding Reference registry (0 members in a 0 member file)
Rebuilding Style registry (0 members in a 0 member file)
Rebuilding Script registry (0 members in a 0 member file)
Rebuilding Program registry (0 members in a 0 member file)
Rebuilding Ole registry (0 members in a 0 member file)
Rebuilding Video registry (0 members in a 0 member file)
Rebuilding Image registry (81 members in a 82 member file)
Rebuilding Sound registry (0 members in a 0 member file)
Rebuilding Tasklist registry (1 members in a 1 member file)
Rebuilding Concept registry (3 members in a 3 member file)
Rebuilding Link registry (0 members in a 0 member file)
Registering components
Transferring templates
Registering references
Registering image references
Registration of links for 0 members in a 0 member file
Registration of concepts for 3 members in a 3 member file
Verifying Text registry
Warning! Registry member not used
   Text #3: HTML: <P>&nbsp;</P>
Warning! Registry member not used
   Text #100: HTML: <P>&nbsp;</P>. <P>&nbsp;</P>
2 unused member(s) in the Text registry
Verifying Font registry
Verifying Reference registry
Verifying Ole registry
Verifying Image registry
Verifying Sound registry
Verifying Video registry
Verifying Style registry
Verifying Script registry
Verifying Program registry
Verifying Template registry
Verifying Tasklist registry
Warning! Registry member not used
   Tasklist #1: Tasks
1 unused member(s) in the Tasklist registry
Verifying Concept registry
Verifying Link registry
Verifying OLE filespace
Verifying image filespace
Verifying sound filespace
Verifying video filespace
Verifying stylesheet filespace
Verifying script filespace
Verifying binary filespace
Verifying text filespace
Inspecting 258 filespace slots
Unused filespace slot count: 2
Checking for empty filespace slots
Inspecting 2 empty filespace slot(s)
Deleting directory: c:\supermemo\systems\math\registry\lexicon_users
Building new lexicon: 497 texts
Warning!
   Changed registry size
   Registry:      Lexicon
   Size reported:    352
   Size found:  358
Verifying repetition history
Repetition history compressed from 19.812 kB to 19.773 kB
Deleting temporary folders
Deleting directory: c:\supermemo\systems\math\temp
Removing empty collection directories

Process completed at 2:54:21 PM in 00:00:03 sec (Friday, November 26, 2021)

NO ERRORS (Checking the integrity of the collection)

____________________________________________________________
