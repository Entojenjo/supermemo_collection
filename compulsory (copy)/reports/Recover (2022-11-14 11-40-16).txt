SuperMemo Report
   Activity: Checking the integrity of the collection
   Collection: c:\users\memory\supermemo\systems\compulsory
   Date: Monday, November 14, 2022, 11:40:16 AM
   SuperMemo 18 (Build 18.05, Oct 29, 2020)

Checking 1,843 element records (ElementInfo.dat)
Turning on tasklist verification along element data integrity checks
Verifying integrity of element data (1843 elements in ElementInfo.dat)
Verifying knowledge tree (1,843 elements in Contents.dat)
Verifying the priority queue of 1,042 elements
Verifying repetition schedule (Workload.dat and ElementInfo.dat)
Verifying pending queue (Intact.dat)
Verifying A-Factor, Lapses and Repetitions distributions
Recovering registries and filespace references
Rebuilding Text registry (2382 members in a 4743 member file)
Rebuilding Template registry (24 members in a 26 member file)
Rebuilding Font registry (0 members in a 0 member file)
Rebuilding Reference registry (85 members in a 253 member file)
Rebuilding Style registry (0 members in a 0 member file)
Rebuilding Script registry (0 members in a 0 member file)
Rebuilding Program registry (133 members in a 223 member file)
Rebuilding Ole registry (0 members in a 0 member file)
Rebuilding Video registry (0 members in a 0 member file)
Rebuilding Image registry (99 members in a 147 member file)
Rebuilding Sound registry (0 members in a 0 member file)
Rebuilding Tasklist registry (1 members in a 3 member file)
Rebuilding Concept registry (19 members in a 61 member file)
Rebuilding Link registry (4 members in a 9 member file)
Registering components
Transferring templates
Registering references
Registering image references
Registration of links for 4 members in a 9 member file
Registration of concepts for 19 members in a 61 member file
Verifying Text registry
Warning! Registry member not used
   Text #3: HTML: <P>&nbsp;</P>
Warning! Registry member not used
   Text #100: HTML: <P>&nbsp;</P>. <P>&nbsp;</P>
Warning! Registry member not used
   Text #965: DE&LA-16-550-1-11.pdf. DE&LA-16-550-1-11.pdf. eyJCTSI6MTM5LCJTUCI6LTEsIkVQIjotMSwiU0kiOi0x ...
Warning! Registry member not used
   Text #1437: Calculus Volume 2 - Chapter 5.5 Alternating Series.pdf. Calculus Volume 2 - Chapter 5.5 Alternating Series.pdf ...
Warning! Registry member not used
   Text #1958: 12.01 _Vectors_in_the_Plane.pdf. 12.01 _Vectors_in_the_Plane.pdf. eyJCTSI6MTcwLCJTUCI6LTEs ...
Warning! Registry member not used
   Text #2666: DE&LA-16-550-11-25.pdf. DE&LA-16-550-11-25.pdf. eyJCTSI6MTQwLCJTUCI6LTEsIkVQIjotMSwiU0kiOi ...
6 unused member(s) in the Text registry
Verifying Font registry
Verifying Reference registry
Verifying Ole registry
Verifying Image registry
Warning! Registry member not used
   Image #139: c:\supermemo4\systems\knowledge\temp\PastedImage13525.jpg
Warning! Registry member not used
   Image #143: 2022:06:03 21:17:36: #995 (KNOWLEDGE) (Component 3)
2 unused member(s) in the Image registry
Verifying Sound registry
Verifying Video registry
Verifying Style registry
Verifying Script registry
Verifying Program registry
Warning! Registry member not used
   Binary #5: 14.04__Tangent_Planes_and_Linear_Approximations.pdf
Warning! Registry member not used
   Binary #7: University Physics Volume 1 - Chapter 5.1 Forces.pdf
Warning! Registry member not used
   Binary #8: University Physics Volume 1 - Chapter 5.2 Newton's First Law.pdf
Warning! Registry member not used
   Binary #9: University Physics Volume 1 - Chapter 5.3 Newton's Second Law.pdf
Warning! Registry member not used
   Binary #10: University Physics Volume 1 - Chapter 5.4 Mass and Weight.pdf
Warning! Registry member not used
   Binary #11: University Physics Volume 1 - Chapter 5.5 Newton's Third Law.pdf
Warning! Registry member not used
   Binary #12: University Physics Volume 1 - Chapter 5.6 Common Forces.pdf
Warning! Registry member not used
   Binary #13: University Physics Volume 1 - Chapter 5.7 Drawing Free-Body Diagrams.pdf
Warning! Registry member not used
   Binary #15: University Physics Volume 1 - Chapter 6.1 - Solving Problems with Newton's Laws.pdf
Warning! Registry member not used
   Binary #16: University Physics Volume 1 - Chapter 6.2 - Friction.pdf
Warning! Registry member not used
   Binary #22: DE&LA-16-550-177-194.pdf
Warning! Registry member not used
   Binary #23: University Physics Volume 1 - Chapter 6.3 - Centripetal Force.pdf
Warning! Registry member not used
   Binary #24: University Physics Volume 1 - Chapter 6.4 - Drag Force and Terminal Speed.pdf
Warning! Registry member not used
   Binary #25: University Physics Volume 1 - Chapter 7 - Work.pdf
Warning! Registry member not used
   Binary #26: University Physics Volume 1 - Chapter 7.1 - Work.pdf
Warning! Registry member not used
   Binary #27: University Physics Volume 1 - Chapter 7.2 - Kinetic Energy.pdf
Warning! Registry member not used
   Binary #28: University Physics Volume 1 - Chapter 7.3 - Work-Energy Theorem.pdf
Warning! Registry member not used
   Binary #29: University Physics Volume 1 - Chapter 7.4 - Power.pdf
Warning! Registry member not used
   Binary #30: University Physics Volume 1 - Chapter 7 Review.pdf
Warning! Registry member not used
   Binary #31: Logical Equivalences Sheet.pdf
Warning! Registry member not used
   Binary #32: Logical Constants.pdf
Warning! Registry member not used
   Binary #33: Algebra-and-Trigonometry-2e-WEB.pdf
Warning! Registry member not used
   Binary #34: Engineering Mechanics_ Statics & Dynamics (2015, Pearson).pdf
Warning! Registry member not used
   Binary #35: Principles of Mathematical Analysis by Walter Rudin.pdf
Warning! Registry member not used
   Binary #36: amd64archvol1.pdf
Warning! Registry member not used
   Binary #37: 253666-sdm-vol-2a.pdf
Warning! Registry member not used
   Binary #38: 253665-sdm-vol-1.pdf
Warning! Registry member not used
   Binary #39: 253667-sdm-vol-2b.pdf
Warning! Registry member not used
   Binary #40: 326018-sdm-vol-2c.pdf
Warning! Registry member not used
   Binary #41: 334569-sdm-vol-2d.pdf
Warning! Registry member not used
   Binary #42: 253668-sdm-vol-3a.pdf
Warning! Registry member not used
   Binary #43: 253669-sdm-vol-3b.pdf
Warning! Registry member not used
   Binary #44: 326019-sdm-vol-3c.pdf
Warning! Registry member not used
   Binary #45: 332831-sdm-vol-3d.pdf
Warning! Registry member not used
   Binary #46: 335592-sdm-vol-4.pdf
Warning! Registry member not used
   Binary #47: architecture-instruction-set-extensions-programming-reference.pdf
Warning! Registry member not used
   Binary #76: 14.07__Maxima_Minima_Problems.pdf
Warning! Registry member not used
   Binary #80: DE&LA-16-550-167-177.pdf
Warning! Registry member not used
   Binary #81: DE&LA-16-550-195-210.pdf
Warning! Registry member not used
   Binary #83: DE&LA-16-550-130-146.pdf
Warning! Registry member not used
   Binary #84: DE&LA-16-550-156-167.pdf
Warning! Registry member not used
   Binary #91: 14.01__Functions_of_Several_Variables.pdf
Warning! Registry member not used
   Binary #92: 14.05__The_Chain_Rule_for_Multivariable_Functions.pdf
Warning! Registry member not used
   Binary #95: 14.02__Limits_and_Continuity.pdf
Warning! Registry member not used
   Binary #96: DE&LA-16-550-146-156.pdf
Warning! Registry member not used
   Binary #99: 14.06__Directional_Derivatives_and_the_Gradient.pdf
Warning! Registry member not used
   Binary #109: 14.03__Partial_Derivatives.pdf
Warning! Registry member not used
   Binary #133: 14.08__Lagrange_Multipliers.pdf
Warning! Registry member not used
   Binary #139: DE&LA-16-550-1-11.pdf
Warning! Registry member not used
   Binary #140: DE&LA-16-550-11-25.pdf
Warning! Registry member not used
   Binary #141: DE&LA-16-550-25-33.pdf
Warning! Registry member not used
   Binary #142: DE&LA-16-550-33-46.pdf
Warning! Registry member not used
   Binary #143: DE&LA-16-550-46-54.pdf
Warning! Registry member not used
   Binary #144: DE&LA-16-550-55-63.pdf
Warning! Registry member not used
   Binary #145: Tipler_Llewellyn-19-758-3-11.pdf
Warning! Registry member not used
   Binary #146: Tipler_Llewellyn-19-758-11-17.pdf
Warning! Registry member not used
   Binary #147: Tipler_Llewellyn-19-758-17-29.pdf
Warning! Registry member not used
   Binary #148: Tipler_Llewellyn-19-758-29-41.pdf
Warning! Registry member not used
   Binary #149: Tipler_Llewellyn-19-758-41-45.pdf
Warning! Registry member not used
   Binary #150: Tipler_Llewellyn-19-758-45-64.pdf
Warning! Registry member not used
   Binary #151: Tipler_Llewellyn-19-758-65-70.pdf
Warning! Registry member not used
   Binary #152: Tipler_Llewellyn-19-758-70-81.pdf
Warning! Registry member not used
   Binary #153: Tipler_Llewellyn-19-758-84-97.pdf
Warning! Registry member not used
   Binary #154: Tipler_Llewellyn-19-758-81-84.pdf
Warning! Registry member not used
   Binary #155: Tipler_Llewellyn-19-758-97-114.pdf
Warning! Registry member not used
   Binary #156: Tipler_Llewellyn-19-758-115-119.pdf
Warning! Registry member not used
   Binary #157: Tipler_Llewellyn-19-758-119-127.pdf
Warning! Registry member not used
   Binary #158: Tipler_Llewellyn-19-758-127-133.pdf
Warning! Registry member not used
   Binary #159: Tipler_Llewellyn-19-758-133-146.pdf
Warning! Registry member not used
   Binary #160: Tipler_Llewellyn-19-758-147-150.pdf
Warning! Registry member not used
   Binary #161: Tipler_Llewellyn-19-758-150-159.pdf
Warning! Registry member not used
   Binary #162: Tipler_Llewellyn-19-758-159-169.pdf
Warning! Registry member not used
   Binary #163: Tipler_Llewellyn-19-758-169-174.pdf
Warning! Registry member not used
   Binary #164: DE&LA-16-550-63-73.pdf
Warning! Registry member not used
   Binary #165: DE&LA-16-550-73-80.pdf
Warning! Registry member not used
   Binary #166: DE&LA-16-550-80-87.pdf
Warning! Registry member not used
   Binary #167: DE&LA-16-550-87-100.pdf
Warning! Registry member not used
   Binary #168: DE&LA-16-550-100-114.pdf
Warning! Registry member not used
   Binary #169: DE&LA-16-550-115-130.pdf
Warning! Registry member not used
   Binary #170: 12.01 _Vectors_in_the_Plane.pdf
Warning! Registry member not used
   Binary #171: 12.1E Exercises for Section 12.1 - Mathematics LibreTexts.pdf
Warning! Registry member not used
   Binary #172: 12.1E _Exercises_for_Section_12.1.pdf
Warning! Registry member not used
   Binary #173: 12.02 _Vectors_in_Three_Dimensions.pdf
Warning! Registry member not used
   Binary #174: 12.2E _Exercises_for_Section_12.2.pdf
Warning! Registry member not used
   Binary #175: 12.03 _The_Dot_Product.pdf
Warning! Registry member not used
   Binary #176: 12.3E _Exercises_for_Section_12.3.pdf
Warning! Registry member not used
   Binary #177: 12.04 _The_Cross_Product.pdf
Warning! Registry member not used
   Binary #178: 12.4E _Exercises_for_Section_12.4.pdf
Warning! Registry member not used
   Binary #179: 12.05 _Equations_of_Lines_and_Planes_in_Space.pdf
Warning! Registry member not used
   Binary #180: 12.5E _Exercises_for_Section_12.5.pdf
Warning! Registry member not used
   Binary #181: 12.06 _Quadric_Surfaces.pdf
Warning! Registry member not used
   Binary #182: 12.6E _Exercises_for_Section_12.6.pdf
Warning! Registry member not used
   Binary #183: 12.07 _Cylindrical_and_Spherical_Coordinates.pdf
Warning! Registry member not used
   Binary #184: 12.7E _Exercises_for_Section_12.7.pdf
Warning! Registry member not used
   Binary #185: Tipler_Llewellyn-19-758-174-184.pdf
Warning! Registry member not used
   Binary #186: Tipler_Llewellyn-19-758-185-187.pdf
Warning! Registry member not used
   Binary #187: 13.01__Vector-Valued_Functions_and_Space_Curves.pdf
Warning! Registry member not used
   Binary #188: 13.1E__Exercises_for_Section_13.1.pdf
Warning! Registry member not used
   Binary #189: 13.02__Calculus_of_Vector-Valued_Functions.pdf
Warning! Registry member not used
   Binary #190: 13.2E__Exercises_for_Section_13.2.pdf
Warning! Registry member not used
   Binary #191: 13.03__Arc_Length_and_Curvature.pdf
Warning! Registry member not used
   Binary #192: 13.3E__Exercises_for_Section_13.3.pdf
Warning! Registry member not used
   Binary #193: 13.04__Motion_in_Space.pdf
Warning! Registry member not used
   Binary #194: 13.4E__Exercises_for_Section_13.4.pdf
Warning! Registry member not used
   Binary #195: 13.05__Chapter_13_Review_Exercises.pdf
Warning! Registry member not used
   Binary #196: 12.08__Chapter_12_Review_Exercises.pdf
Warning! Registry member not used
   Binary #197: Outcomes, Events, and Probability.pdf
Warning! Registry member not used
   Binary #198: Conditional Probability and Independence.pdf
Warning! Registry member not used
   Binary #199: Discrete Random Variables.pdf
Warning! Registry member not used
   Binary #200: Continuous Random Variables.pdf
Warning! Registry member not used
   Binary #201: Simulation.pdf
Warning! Registry member not used
   Binary #202: Expectation and Variance.pdf
Warning! Registry member not used
   Binary #203: Computations with Random Variables.pdf
Warning! Registry member not used
   Binary #204: Joint Distributions and Independence.pdf
Warning! Registry member not used
   Binary #205: Covariance and Correlation.pdf
Warning! Registry member not used
   Binary #206: More Computations with More Random Variables.pdf
Warning! Registry member not used
   Binary #207: The Poisson Process.pdf
Warning! Registry member not used
   Binary #208: The Law of Large Numbers.pdf
Warning! Registry member not used
   Binary #209: The Central Limit Theorem.pdf
Warning! Registry member not used
   Binary #210: Exploratory Data Analysis - Graphical Summaries.pdf
Warning! Registry member not used
   Binary #211: Exploratory Data Analysis - Numerical Summaries.pdf
Warning! Registry member not used
   Binary #212: Basic Statistical Models.pdf
Warning! Registry member not used
   Binary #213: The Bootstrap.pdf
Warning! Registry member not used
   Binary #214: Unbiased Estimators.pdf
Warning! Registry member not used
   Binary #215: Efficiency and mean squared error.pdf
Warning! Registry member not used
   Binary #216: Maximum Likelihood.pdf
Warning! Registry member not used
   Binary #217: The Method of Least Squares.pdf
Warning! Registry member not used
   Binary #218: Confidence Intervals for the Mean.pdf
Warning! Registry member not used
   Binary #219: More on Confidence Intervals.pdf
Warning! Registry member not used
   Binary #220: Testing Hypotheses - Essentials.pdf
Warning! Registry member not used
   Binary #221: Testing Hypotheses - Elaboration.pdf
Warning! Registry member not used
   Binary #222: The t-Test.pdf
Warning! Registry member not used
   Binary #223: Comparing Two Samples.pdf
133 unused member(s) in the Program registry
Verifying Template registry
Verifying Tasklist registry
Warning! Registry member not used
   Tasklist #2: General
1 unused member(s) in the Tasklist registry
Verifying Concept registry
Verifying Link registry
Deleting directory: c:\users\memory\supermemo\systems\compulsory\registry\Text_users
Deleting directory: c:\users\memory\supermemo\systems\compulsory\registry\Template_users
Verifying OLE filespace
Verifying image filespace
Verifying sound filespace
Verifying video filespace
Verifying stylesheet filespace
Verifying script filespace
Verifying binary filespace
Verifying text filespace
Inspecting 2159 filespace slots
Unused filespace slot count: 782
Checking for empty filespace slots
Inspecting 782 empty filespace slot(s)
Deleting directory: c:\users\memory\supermemo\systems\compulsory\registry\lexicon_users
Building new lexicon: 4,743 texts
Warning!
   Changed registry size
   Registry:      Lexicon
   Size reported:    6601
   Size found:  6603
Verifying repetition history
Repetition history compressed from 118.17 kB to 118.17 kB
Deleting temporary folders
Deleting directory: c:\users\memory\supermemo\systems\compulsory\temp
The size of remaining temporary files is 1.218 MB
   These files can be deleted with File : Tools : Garbage
Removing empty collection directories
Deleting directory: c:\users\memory\supermemo\systems\compulsory\sma\configs\SuperMemoAssistant.Plugins.LaTeX\
Deleting directory: c:\users\memory\supermemo\systems\compulsory\sma\configs\SuperMemoAssistant.Plugins.PDF\

Process completed at 11:41:15 AM in 00:00:59 sec (Monday, November 14, 2022)

NO ERRORS (Checking the integrity of the collection)

____________________________________________________________
