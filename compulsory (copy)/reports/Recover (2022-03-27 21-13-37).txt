SuperMemo Report
   Activity: Checking the integrity of the collection
   Collection: c:\supermemo\systems\sciences
   Date: Sunday, March 27, 2022, 9:13:37 PM
   SuperMemo 18 (Build 18.05, Oct 29, 2020)

Checking 649 element records (ElementInfo.dat)
Turning on tasklist verification along element data integrity checks
Verifying integrity of element data (649 elements in ElementInfo.dat)
Verifying knowledge tree (649 elements in Contents.dat)
Verifying the priority queue of 637 elements
Verifying repetition schedule (Workload.dat and ElementInfo.dat)
Verifying pending queue (Intact.dat)
Verifying A-Factor, Lapses and Repetitions distributions
Recovering registries and filespace references
Rebuilding Text registry (1261 members in a 1508 member file)
Rebuilding Template registry (21 members in a 22 member file)
Rebuilding Font registry (0 members in a 0 member file)
Rebuilding Reference registry (2 members in a 2 member file)
Rebuilding Style registry (0 members in a 0 member file)
Rebuilding Script registry (0 members in a 0 member file)
Rebuilding Program registry (2 members in a 2 member file)
Rebuilding Ole registry (0 members in a 0 member file)
Rebuilding Video registry (0 members in a 0 member file)
Rebuilding Image registry (98 members in a 101 member file)
Rebuilding Sound registry (0 members in a 0 member file)
Rebuilding Tasklist registry (1 members in a 1 member file)
Rebuilding Concept registry (18 members in a 18 member file)
Rebuilding Link registry (7 members in a 7 member file)
Registering components
Transferring templates
Registering references
Registering image references
Registration of links for 7 members in a 7 member file
Registration of concepts for 18 members in a 18 member file
Verifying Text registry
Warning! Registry member not used
   Text #3: HTML: <P>&nbsp;</P>
Warning! Registry member not used
   Text #100: HTML: <P>&nbsp;</P>. <P>&nbsp;</P>
2 unused member(s) in the Text registry
Verifying Font registry
Verifying Reference registry
Verifying Ole registry
Verifying Image registry
Verifying Sound registry
Verifying Video registry
Verifying Style registry
Verifying Script registry
Verifying Program registry
Warning! Registry member not used
   Binary #1: Aerokashi.pdf
Warning! Registry member not used
   Binary #2: Calc2-Sequences.pdf
2 unused member(s) in the Program registry
Verifying Template registry
Verifying Tasklist registry
Verifying Concept registry
Verifying Link registry
Verifying OLE filespace
Verifying image filespace
Verifying sound filespace
Verifying video filespace
Verifying stylesheet filespace
Verifying script filespace
Verifying binary filespace
Verifying text filespace
Inspecting 585 filespace slots
Unused filespace slot count: 3
Checking for empty filespace slots
Inspecting 3 empty filespace slot(s)
Verifying repetition history
Repetition history compressed from 78.897 kB to 78.858 kB
Deleting temporary folders
Deleting directory: c:\supermemo\systems\sciences\temp
The size of remaining temporary files is 146.652 kB
   These files can be deleted with File : Tools : Garbage
Removing empty collection directories
Deleting directory: c:\supermemo\systems\sciences\sma\configs\SuperMemoAssistant.Plugins.Dictionary\
Deleting directory: c:\supermemo\systems\sciences\sma\configs\SuperMemoAssistant.Plugins.LaTeX\
Deleting directory: c:\supermemo\systems\sciences\sma\configs\SuperMemoAssistant.Plugins.PDF\

Process completed at 9:13:41 PM in 00:00:03 sec (Sunday, March 27, 2022)

NO ERRORS (Checking the integrity of the collection)

____________________________________________________________
