SuperMemo Report
   Activity: Checking the integrity of the collection
   Collection: c:\supermemo\systems\sciences
   Date: Monday, April 4, 2022, 5:35:12 PM
   SuperMemo 18 (Build 18.05, Oct 29, 2020)

Checking 687 element records (ElementInfo.dat)
Turning on tasklist verification along element data integrity checks
Verifying integrity of element data (687 elements in ElementInfo.dat)
Verifying knowledge tree (687 elements in Contents.dat)
Verifying the priority queue of 665 elements
Verifying repetition schedule (Workload.dat and ElementInfo.dat)
Verifying pending queue (Intact.dat)
Verifying A-Factor, Lapses and Repetitions distributions
Recovering registries and filespace references
Rebuilding Text registry (1332 members in a 1637 member file)
Rebuilding Template registry (21 members in a 22 member file)
Rebuilding Font registry (0 members in a 0 member file)
Rebuilding Reference registry (14 members in a 14 member file)
Rebuilding Style registry (0 members in a 0 member file)
Rebuilding Script registry (0 members in a 0 member file)
Rebuilding Program registry (14 members in a 14 member file)
Rebuilding Ole registry (0 members in a 0 member file)
Rebuilding Video registry (0 members in a 0 member file)
Rebuilding Image registry (107 members in a 110 member file)
Rebuilding Sound registry (0 members in a 0 member file)
Rebuilding Tasklist registry (1 members in a 1 member file)
Rebuilding Concept registry (21 members in a 21 member file)
Rebuilding Link registry (7 members in a 7 member file)
Registering components
Transferring templates
Registering references
Registering image references
Registration of links for 7 members in a 7 member file
Registration of concepts for 21 members in a 21 member file
Verifying Text registry
Warning! Registry member not used
   Text #3: HTML: <P>&nbsp;</P>
Warning! Registry member not used
   Text #100: HTML: <P>&nbsp;</P>. <P>&nbsp;</P>
++++++++++++++++++++++    ERROR #1   ++++++++++++++++++++++
   Wrong reference count for Text #1437: Calculus Volume 2 - Chapter 5.5 Alternating Series.pdf. Calculus Volume 2 - Chapter 5.5 Alternating Series.pdf ...
   Count: reported=1 found=0
   Registry: text
Usership count difference at Text #1437: Calculus Volume 2 - Chapter 5.5 Alternating Series.pdf. Calculus Volume 2 - Chapter 5.5 Alternating Series.pdf ...
Element List=0 users
Size=1
2 unused member(s) in the Text registry
Verifying Font registry
Verifying Reference registry
Verifying Ole registry
Verifying Image registry
Verifying Sound registry
Verifying Video registry
Verifying Style registry
Verifying Script registry
Verifying Program registry
Warning! Registry member not used
   Binary #1: Aerokashi.pdf
Warning! Registry member not used
   Binary #2: Calc2-Sequences.pdf
Warning! Registry member not used
   Binary #3: Calculus Volume 2 - Chapter 5.2 Series.pdf
Warning! Registry member not used
   Binary #4: Calculus Volume 2 - Chapter 5.4 Comparison Tests.pdf
Warning! Registry member not used
   Binary #5: Calculus Volume 2 - Chapter 5.3 The Divergence and Integral Tests.pdf
Warning! Registry member not used
   Binary #6: Calculus Volume 2 - Chapter 5.5 Alternating Series.pdf
Warning! Registry member not used
   Binary #7: University Physics Volume 1 - Chapter 5.1 Forces.pdf
Warning! Registry member not used
   Binary #8: University Physics Volume 1 - Chapter 5.2 Newton's First Law.pdf
Warning! Registry member not used
   Binary #9: University Physics Volume 1 - Chapter 5.3 Newton's Second Law.pdf
Warning! Registry member not used
   Binary #10: University Physics Volume 1 - Chapter 5.4 Mass and Weight.pdf
Warning! Registry member not used
   Binary #11: University Physics Volume 1 - Chapter 5.5 Newton's Third Law.pdf
Warning! Registry member not used
   Binary #12: University Physics Volume 1 - Chapter 5.6 Common Forces.pdf
Warning! Registry member not used
   Binary #13: University Physics Volume 1 - Chapter 5.7 Drawing Free-Body Diagrams.pdf
Warning! Registry member not used
   Binary #14: Calculus Volume 2 - Chapter 5.6 Ratio and Root Tests.pdf
14 unused member(s) in the Program registry
Verifying Template registry
Verifying Tasklist registry
Verifying Concept registry
Verifying Link registry
Verifying OLE filespace
Verifying image filespace
Verifying sound filespace
Verifying video filespace
Verifying stylesheet filespace
Verifying script filespace
Verifying binary filespace
Verifying text filespace
Inspecting 620 filespace slots
Unused filespace slot count: 3
Checking for empty filespace slots
Inspecting 3 empty filespace slot(s)
Deleting directory: c:\supermemo\systems\sciences\registry\lexicon_users
Building new lexicon: 1,637 texts
Verifying repetition history
Repetition history compressed from 81.042 kB to 81.042 kB
Deleting temporary folders
Deleting directory: c:\supermemo\systems\sciences\temp
The size of remaining temporary files is 162.34 kB
   These files can be deleted with File : Tools : Garbage
Removing empty collection directories

Process completed at 5:35:27 PM in 00:00:15 sec (Monday, April 4, 2022)

ONE ERROR (Checking the integrity of the collection)

____________________________________________________________
